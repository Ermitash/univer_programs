#pragma once
#include <iostream>
using namespace std;
class Queue
{
public:
	Queue();
	void put(int number);
	int get();
	void show_massiv();
private:
	int first_tail=0;
	int first_head = 1;
	int head=0;
	int tail=0;
	static const int MAX = 10;
	int qu[MAX];
};