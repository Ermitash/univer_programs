#include <iostream>
#include "Queue.h"
using namespace std;

int main()
{
	setlocale(LC_ALL, "Russian");
	Queue a;
	a.get();
	a.put(23);
	a.put(25);
	a.show_massiv();
	a.get();
	a.get();
	a.show_massiv();
	a.get();
	a.put(15);
	a.put(16);
	a.put(17);
	a.put(18);
	a.put(19);
	a.put(20);
	a.put(21);
	a.put(22);
	a.put(23);
	a.put(24);
	a.show_massiv();
	a.put(25);
	a.get();
	a.put(25);
	a.show_massiv();
	system("pause");
	return 0;
}