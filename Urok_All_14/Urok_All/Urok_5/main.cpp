#include <Windows.h>
#include <iostream>
#include "ring.h"

HWND hWnd=NULL;
int window_size_x = 1366;
int windows_size_y = 768;
LRESULT CALLBACK WndProc(HWND hWnd,UINT lpCmdLine,WPARAM wparam,LPARAM lparam);
LRESULT InitWindow(HINSTANCE hInstance, int nCmdShow);
HINSTANCE hInstance_2 = NULL;

int WinMain(HINSTANCE hInstance, HINSTANCE PrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	hInstance_2 = hInstance;
	InitWindow( hInstance,nCmdShow);

	MSG msg = { 0 };

	while (msg.message!=WM_QUIT)
	{
		if (GetMessage(&msg, NULL,0, 0) != 0)
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	

	return (int)msg.wParam;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT lpCmdLine, WPARAM wparam, LPARAM lparam)
{
	PAINTSTRUCT ps;
	HDC hDeviceContext=NULL;
	HPEN hPen=NULL;
	COLORREF color[] = { RGB(255, 0, 0), RGB(0, 255, 0),RGB(0,0,255) };
	POINT center = { 250,250 };
	int radius = 200;
	int tolchina = 400;
	circle circle1(color[0],center, radius);
	Ring* ring;
	switch (lpCmdLine)
	{
	case WM_PAINT:
	    hDeviceContext = BeginPaint(hWnd, &ps);
	
	    ring=new Ring(color, center, radius, hWnd, PS_SOLID, window_size_x, windows_size_y, false, hDeviceContext, hPen,tolchina);
		EndPaint(hWnd, &ps);
		delete ring;
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		ReleaseDC(hWnd, hDeviceContext);
		DeleteObject(hPen);
		break;
	default:
		return DefWindowProc(hWnd, lpCmdLine, wparam, lparam);
	}
}

LRESULT InitWindow(HINSTANCE hInstance, int nCmdShow)
{
	WNDCLASSEX wnd;
	wnd.cbSize = sizeof(WNDCLASSEX);
	wnd.cbClsExtra = 0;
	wnd.cbWndExtra = 0;
	wnd.hCursor = NULL;
	wnd.hIcon = NULL;
	wnd.hIconSm = NULL;
	wnd.hbrBackground = (HBRUSH)COLOR_WINDOW;
	wnd.hInstance = hInstance;
	wnd.lpfnWndProc = WndProc;
	wnd.lpszClassName = "wnd";
	wnd.style = CS_HREDRAW | CS_VREDRAW;
	wnd.lpszMenuName = NULL;

	if (!RegisterClassEx(&wnd))
		return E_FAIL;

	hWnd = CreateWindow("wnd", "Urok_1", WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, CW_USEDEFAULT, window_size_x, windows_size_y, hWnd, NULL, hInstance, NULL);

	if (!hWnd)
		return E_FAIL;

	UpdateWindow(hWnd);
	ShowWindow(hWnd, nCmdShow);
}