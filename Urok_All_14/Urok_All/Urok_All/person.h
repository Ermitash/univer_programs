#pragma once
#include <iostream>
#include <string>
using namespace std;

class person
{
public:
	person();
	void setName();
    string getName();
	void setSalary();
	float getSalary();
	void sort_list(person **p, int n);
	void show(person **p, int n);
private:
	string name;
	float salary;
};