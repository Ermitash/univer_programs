#include "Stack.h"

Stack::Stack(COLORREF color[], POINT center, int radius, HWND hWnd, int iStyle, int window_x, int window_y, bool paint, HDC hDeviceContext, HPEN hPen)
{
	circle circle1(color[0],center,radius);
	circle1.draw_circle(hWnd, PS_SOLID, window_x, window_y, paint, hDeviceContext, hPen);
	circle1.set_circle(radius - radius * 0.1, center, color[1]);
	circle1.draw_circle(hWnd, PS_SOLID, window_x, window_y, paint, hDeviceContext, hPen);
	circle1.set_circle(radius - radius * 0.2, center, color[2]);
	circle1.draw_circle(hWnd, PS_SOLID, window_x, window_y, paint, hDeviceContext, hPen);
	circle1.set_circle(radius - radius * 0.3, center, color[0]);
	circle1.draw_circle(hWnd, PS_SOLID, window_x, window_y, paint, hDeviceContext, hPen);
	circle1.set_circle(radius - radius * 0.4, center, color[1]);
	circle1.draw_circle(hWnd, PS_SOLID, window_x, window_y, paint, hDeviceContext, hPen);
	circle1.set_circle(radius - radius * 0.5, center, color[2]);
	circle1.draw_circle(hWnd, PS_SOLID, window_x, window_y, paint, hDeviceContext, hPen);
	circle1.set_circle(radius - radius * 0.6, center, color[0]);
	circle1.draw_circle(hWnd, PS_SOLID, window_x, window_y, paint, hDeviceContext, hPen);
	circle1.set_circle(radius - radius * 0.7, center, color[1]);
	circle1.draw_circle(hWnd, PS_SOLID, window_x, window_y, paint, hDeviceContext, hPen);
	circle1.set_circle(radius - radius * 0.8, center, color[2]);
	circle1.draw_circle(hWnd, PS_SOLID, window_x, window_y, paint, hDeviceContext, hPen);
	circle1.set_circle(radius - radius * 0.9, center, color[0]);
	circle1.draw_circle(hWnd, PS_SOLID, window_x, window_y, paint, hDeviceContext, hPen);
}
