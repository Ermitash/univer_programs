#include "circle.h"

circle::circle(COLORREF color, POINT center, int radius)
{
	set_circle(radius, center, color);
}

void circle::set_circle(int radius, POINT center, COLORREF color)
{
	circle::radius = radius;
	circle::center = center;
	circle::color = color;
}

void circle::draw_circle(HWND hWnd, int iStyle, int window_x, int window_y, bool paint, HDC hDeviceContext, HPEN hPen)
{
	hDeviceContext = GetDC(hWnd);
	hPen = CreatePen(iStyle, 1, color);
	SelectObject(hDeviceContext, hPen);

	POINT coord = { 0,0 };
	MoveToEx(hDeviceContext, coord.x, coord.y, NULL);

	if (paint == true)
	{
		while (coord.y <= window_y)
		{
			if ((((coord.x - center.x)*(coord.x - center.x) + (coord.y - center.y)*(coord.y - center.y)) - radius*radius) < 0)
			{
				LineTo(hDeviceContext, coord.x + 1, coord.y);
				coord.x++;
				MoveToEx(hDeviceContext, coord.x, coord.y, NULL);

			}
			else
			{
				if (coord.x >= window_x)
				{
					coord.x = 0;
					coord.y++;
					MoveToEx(hDeviceContext, coord.x, coord.y, NULL);
				}
				else
					coord.x++;
				MoveToEx(hDeviceContext, coord.x, coord.y, NULL);
			}
		}
	}
	else
	{
		hPen = CreatePen(iStyle, 1, color);
		SelectObject(hDeviceContext, hPen);

		while (coord.y <= window_y)
		{
			if ((((coord.x - center.x)*(coord.x - center.x) + (coord.y - center.y)*(coord.y - center.y)) - radius*radius) >= 0 && (((coord.x - center.x)*(coord.x - center.x) + (coord.y - center.y)*(coord.y - center.y)) - radius*radius)<100)
			{
				LineTo(hDeviceContext, coord.x + 1, coord.y);
				coord.x++;
				MoveToEx(hDeviceContext, coord.x, coord.y, NULL);
			}
			else
			{
				if (coord.x >= window_x)
				{
					coord.x = 0;
					coord.y++;
					MoveToEx(hDeviceContext, coord.x, coord.y, NULL);
				}
				else
					coord.x++;
				MoveToEx(hDeviceContext, coord.x, coord.y, NULL);
			}
		}
	}
}

POINT circle::get_center()
{
	return center;
}
int circle::get_radius()
{
	return radius;
}
COLORREF circle::get_color()
{
	return color;
}