#include "Book.h"
#include "tape.h"

int main()
{
	setlocale(LC_ALL, "Russian");

	tape *one = new tape();
	book *two = new book();

	one->putdata();
	cout << endl;
	two->putdata();
	cout << endl;
	cout << endl;
	one->getdata();
	cout << endl;
	two->getdata();

	system("pause");
	return 0;
}