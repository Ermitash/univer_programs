#include "Move_rect.h"


move_rect::move_rect(HWND hwnd, HDC hDeviceContext, HPEN hPen, COORD center, color color, COORD height, COORD width)
	:rect(hwnd,hDeviceContext,hPen,center,color,height,width)
{

}


void move_rect::increase(color* color_type)
{
	COORD left_b = left_bottom;
	COORD right_t = right_top;

	left_b.X -= 126;
	left_b.Y += 77;
	right_t.X += 126;
	right_t.Y -= 77;


	set_rect(left_b, right_t, *color_type);
	draw();
}

void move_rect::decrease(color* color_type)
{

	COORD left_b = left_bottom;
	COORD right_t = right_top;

	left_b.X += 126;
	left_b.Y -= 77;
	right_t.X -= 126;
	right_t.Y += 77;

	set_rect(left_b, right_t, *color_type);
	draw();
}
