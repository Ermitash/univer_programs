#include "rect.h"

void rect::set_rect(COORD left_bottom, COORD right_top, color color_type)
{
	rect::left_bottom = left_bottom;
	rect::right_top = right_top;
	setColor(color_type);
}

void rect::draw_fon()
{
	COORD left_b;
	COORD right_t;

	left_b = { 0 ,size_screen.Y };
	right_t = { size_screen.X, 0 };

	color fon = { 0,0,255 };
	set_rect(left_b, right_t, fon);
	hPen = CreatePen(PS_SOLID, 0.9, RGB(fon.R, fon.G, fon.B));
	SelectObject(hdc, hPen);
	draw();
}

void rect::draw()
{
	hPen = CreatePen(PS_SOLID, 1.0, RGB(color_type.R, color_type.G, color_type.B));
	SelectObject(hdc, hPen);

	for (int i = right_top.Y; i <= left_bottom.Y; i++)
	{
		MoveToEx(hdc, left_bottom.X, i, NULL);
		LineTo(hdc, left_bottom.X + (right_top.X-left_bottom.X), i);

	}

}

rect::rect(HWND hwnd, HDC hDeviceContext, HPEN hPen,COORD center,color color, COORD height, COORD width)
	:shape()
{
	setHWND(hwnd);
	setHDC(hDeviceContext);
	sethPen(hPen);

	size_screen.X = GetSystemMetrics(SM_CXSCREEN);
	size_screen.Y = GetSystemMetrics(SM_CYSCREEN);

	COORD left_b;
	COORD right_t;

	left_b = { 0 ,size_screen.Y };
	right_t = { size_screen.X, 0 };

	center.X = center.X ;
	center.Y = center.Y ;
	setCenter(center);

	left_b.X = center.X - width.X / 2;
	left_b.Y = center.Y + height.Y / 2;
	right_t.X = center.X + width.X / 2;
	right_t.Y = center.Y - height.Y / 2;

	set_rect(left_b, right_t, color);
}

