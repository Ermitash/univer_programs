#include <Windows.h>
#include <iostream>
#include "ellipse.h"
#include "circle.h"
#include "Move_rect.h"
#include "rect.h"



HWND hWnd = NULL;
int window_size_x = 1366;
int windows_size_y = 768;
LRESULT CALLBACK WndProc(HWND hWnd, UINT lpCmdLine, WPARAM wparam, LPARAM lparam);
LRESULT InitWindow(HINSTANCE hInstance, int nCmdShow);
HINSTANCE hInstance_2 = NULL;

int WinMain(HINSTANCE hInstance, HINSTANCE PrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	hInstance_2 = hInstance;
	InitWindow(hInstance, nCmdShow);

	MSG msg = { 0 };

	while (msg.message != WM_QUIT)
	{
		if (GetMessage(&msg, NULL, 0, 0) != 0)
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}



	return (int)msg.wParam;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT lpCmdLine, WPARAM wparam, LPARAM lparam)
{
	PAINTSTRUCT ps;
	HDC hDeviceContext = NULL;
	HPEN hPen = NULL;
	color color1[] = { { 100,0,255 } ,{255, 0, 0}, {0, 100, 50},{ 100,50,255 },{ 255,255,0 },{ 50,0,255 },{ 70,100,255 },{ 200,150,255 } };
	 LPCSTR name[] = { "���������� ������", "���������", "�����", "��� �� �������", "������� ����", "������������", "���-1" };
	 int simbols[] = { 18,10,6,15,13,14,6 };
	int begin_sector[] = { 0,108,144,162,216,306,324 };
	int   end_sector[] = { 108,144,162,216,306,324,360 };
	COORD center = { 500.0,300.0 };
	double radius = 100;
	double radius2 = 180;
	rect* rect1;
	circle *circle1;


	LOGFONT lf;
	HFONT hFont;

	lf.lfHeight = 30;
	lf.lfItalic = 1;
	lf.lfStrikeOut = 0;
	lf.lfUnderline = 0;
	lf.lfWidth = 10;
	lf.lfWeight = 30;
	lf.lfCharSet = DEFAULT_CHARSET; //�������� �� ���������
	lf.lfPitchAndFamily = DEFAULT_PITCH; //�������� �� ���������
	lf.lfEscapement = 0;

	short start_x = 825;
	short start_y = 200;

	switch (lpCmdLine)
	{
	case WM_KEYDOWN:
		DestroyWindow(hWnd);
		break;
	case WM_PAINT:
		hDeviceContext= BeginPaint(hWnd, &ps);
		rect1 = new rect(hWnd, hDeviceContext, hPen, {680,384}, color1[0], { 0,768 }, { 1366,0 });
		rect1->draw();
		delete rect1;

		hFont = CreateFontIndirect(&lf);
		SetTextColor(hDeviceContext, RGB(255, 242, 0));
		SetBkMode(hDeviceContext, TRANSPARENT);
		SelectObject(hDeviceContext, hFont);
		TextOut(hDeviceContext, 800, 100, "��������� ��������������� ��������", 35);

		for (int i = 1; i < 8; i++)
		{
			circle1 = new circle(hWnd, hDeviceContext, hPen, 1366, 768, color1[i], center, begin_sector[i-1], end_sector[i-1], radius2);
			circle1->setiStyle(PS_SOLID);
			circle1->setPaint(true);
			circle1->draw_sector();
			delete circle1;
		}

	
		for (int i = 1; i < 8; i++)
		{
			rect1 = new rect(hWnd, hDeviceContext, hPen, { start_x,start_y }, color1[i], { 0,20 }, { 40,0 });
			rect1->draw();
			delete rect1;
			start_y += 40;
		}

		lf.lfHeight = 20;
		lf.lfWidth = 8;
		lf.lfItalic = 0;
		strcpy_s(lf.lfFaceName, "Times New Roman");
		for (int i = 1; i < 8; i++)
		{
			hFont = CreateFontIndirect(&lf);
			SetTextColor(hDeviceContext, RGB(255, 200, 0));
			SetBkMode(hDeviceContext, TRANSPARENT);
			SelectObject(hDeviceContext, hFont);
			TextOut(hDeviceContext, start_x+60, start_y-290, name[i-1], simbols[i-1]);
			start_y += 40;
		}

		EndPaint(hWnd, &ps);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		ReleaseDC(hWnd, hDeviceContext);
		DeleteObject(hPen);
		break;
	default:
		return DefWindowProc(hWnd, lpCmdLine, wparam, lparam);
	}
}

LRESULT InitWindow(HINSTANCE hInstance, int nCmdShow)
{
	WNDCLASSEX wnd;
	wnd.cbSize = sizeof(WNDCLASSEX);
	wnd.cbClsExtra = 0;
	wnd.cbWndExtra = 0;
	wnd.hCursor = NULL;
	wnd.hIcon = NULL;
	wnd.hIconSm = NULL;
	wnd.hbrBackground = (HBRUSH)COLOR_WINDOW;
	wnd.hInstance = hInstance;
	wnd.lpfnWndProc = WndProc;
	wnd.lpszClassName = "wnd";
	wnd.style = CS_HREDRAW | CS_VREDRAW;
	wnd.lpszMenuName = NULL;

	if (!RegisterClassEx(&wnd))
		return E_FAIL;

	hWnd = CreateWindow("wnd", "Urok_1", WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, CW_USEDEFAULT, window_size_x, windows_size_y, hWnd, NULL, hInstance, NULL);

	if (!hWnd)
		return E_FAIL;

	UpdateWindow(hWnd);
	ShowWindow(hWnd, nCmdShow);
}