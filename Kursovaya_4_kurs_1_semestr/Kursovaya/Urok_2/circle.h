#pragma once
#include <iostream>
#include <Windows.h>
#include <conio.h>
#include "Shape.h"
#include <math.h>

class circle: protected shape
{
public:
	circle(HWND hwnd, HDC hDeviceContext, HPEN hPen, int window_x, int window_y, color color, COORD center, double radius =100);
	circle(HWND hwnd, HDC hDeviceContext, HPEN hPen, int window_x, int window_y, color color, COORD center,  int begin_ugol, int end_ugol,double radius = 100);
	virtual void draw();
	void draw_sector();
	void set_radius(double radius);
	double get_radius();
	void setiStyle(int iStyle);
	int getiStyle();
	void setPaint(bool paint);
	bool getPaint();
	void setWindowX(int window_x);
	int getWindowX();
	void setWindowY(int window_y);
	int getWindowY();
	void set_circle(double radius1, COORD center, color color);
private:
	double radius;
	int iStyle;
	bool paint;
	int window_x;
	int window_y;
	double begin_ugol;
	double end_ugol;
};