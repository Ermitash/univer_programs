#include "Shape.h"

shape::shape()
{
}

void shape::setColor(color color1)
{
	color_type = color1;
}

color shape::getColor()
{
	return color_type;
}

void shape::setCenter(COORD center)
{
	shape::center = center;
}

COORD shape::getCenter()
{
	return center;
}

void shape::setHWND(HWND hwnd)
{
	shape::hwnd = hwnd;
}

HWND shape::getHWND()
{
	return hwnd;
}

void shape::setHDC(HDC hdc)
{
	shape::hdc = hdc;
}

HDC shape::getHDC()
{
	return hdc;
}

void shape::sethPen(HPEN hPen)
{
	shape::hPen = hPen;
}

HPEN shape::gethPen()
{
	return hPen;
}
