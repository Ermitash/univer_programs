#include "circle.h"

circle::circle(HWND hwnd, HDC hDeviceContext, HPEN hPen, int window_x, int window_y,color color, COORD center, double radius)
{
	setHWND(hwnd);
	setHDC(hDeviceContext);
	sethPen(hPen);
	setCenter(center);
	set_radius(radius);
	setColor(color);
	setWindowX(window_x);
	setWindowY(window_y);
}

void circle::draw()
{
	setHDC(GetDC(hwnd));
	sethPen(CreatePen(iStyle, 1, RGB(getColor().R, getColor().G, getColor().B)));
	SelectObject(getHDC(), gethPen());

	COORD coord = { 0,0 };
	MoveToEx(getHDC(), coord.X, coord.Y, NULL);

	if (paint == true)
	{
		while (coord.Y <= window_y)
		{
			if ((((coord.X - center.X)*(coord.X - center.X) + (coord.Y - center.Y)*(coord.Y - center.Y)) - radius*radius) < 0)
			{
				LineTo(getHDC(), coord.X + 1, coord.Y);
				coord.X++;
				MoveToEx(getHDC(), coord.X, coord.Y, NULL);
			}
			else
			{
				if (coord.X >= window_x)
				{
					coord.X = 0;
					coord.Y++;
					MoveToEx(getHDC(), coord.X, coord.Y, NULL);
				}
				else
					coord.X++;
				MoveToEx(getHDC(), coord.X, coord.Y, NULL);
			}
		}
	}
	else
	{
		sethPen(CreatePen(iStyle, 1, RGB(color_type.R, color_type.G, color_type.B)));
		SelectObject(getHDC(), gethPen());

		while (coord.Y <= window_y)
		{
			if ((((coord.X - center.X)*(coord.X - center.X) + (coord.Y - center.Y)*(coord.Y - center.Y)) - radius*radius) >= 0 && (((coord.X - center.X)*(coord.X - center.X) + (coord.Y - center.Y)*(coord.Y - center.Y)) - radius*radius)<100)
			{
				LineTo(getHDC(), coord.X + 1, coord.Y);
				coord.X++;
				MoveToEx(getHDC(), coord.X, coord.Y, NULL);
			}
			else
			{
				if (coord.X >= window_x)
				{
					coord.X = 0;
					coord.Y++;
					MoveToEx(getHDC(), coord.X, coord.Y, NULL);
				}
				else
					coord.X++;
				MoveToEx(getHDC(), coord.X, coord.Y, NULL);
			}
		}
	}
}

void circle::set_radius(double radius)
{
	circle::radius = radius;
}

double circle::get_radius()
{
	return radius;
}

void circle::setiStyle(int iStyle)
{
	circle::iStyle = iStyle;
}

int circle::getiStyle()
{
	return iStyle;
}

void circle::setPaint(bool paint)
{
	circle::paint = paint;
}

bool circle::getPaint()
{
	return paint;
}

void circle::setWindowX(int window_x)
{
	circle::window_x = window_x;
}

int circle::getWindowX()
{
	return window_x;
}

void circle::setWindowY(int window_y)
{
	circle::window_y = window_y;
}

int circle::getWindowY()
{
	return window_y;
}

void circle::set_circle(double radius1, COORD center, color color)
{
	set_radius(radius1);
	setCenter(center);
	setColor(color);
}
