#pragma once
#include <iostream>
#include <Windows.h>
#include "Shape.h"
using namespace std;

class rect: public shape
{
protected:
	COORD size_screen;
	COORD left_bottom;
	COORD right_top;
public:
	rect(HWND hwnd, HDC hDeviceContext, HPEN hPen, COORD center, color color,COORD height = { 0,153 }, COORD width = { 273,0 });
	~rect();
	void set_rect(COORD left_bottom, COORD right_top, color color_type);
	void draw_fon();
	virtual void draw();
	
};