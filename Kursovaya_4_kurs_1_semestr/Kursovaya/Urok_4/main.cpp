#include <Windows.h>
#include <iostream>
#include "ellipse.h"
#include "circle.h"
#include "rect.h"



HWND hWnd = NULL;
int window_size_x = 1366;
int windows_size_y = 768;
LRESULT CALLBACK WndProc(HWND hWnd, UINT lpCmdLine, WPARAM wparam, LPARAM lparam);
LRESULT InitWindow(HINSTANCE hInstance, int nCmdShow);
HINSTANCE hInstance_2 = NULL;

int WinMain(HINSTANCE hInstance, HINSTANCE PrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	hInstance_2 = hInstance;
	InitWindow(hInstance, nCmdShow);

	MSG msg = { 0 };

	while (msg.message != WM_QUIT)
	{
		if (GetMessage(&msg, NULL, 0, 0) != 0)
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}



	return (int)msg.wParam;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT lpCmdLine, WPARAM wparam, LPARAM lparam)
{
	PAINTSTRUCT ps;
	HDC hDeviceContext = NULL;
	HPEN hPen = NULL;
	color color1[] = { {255, 0, 0}, {0, 100, 50},{100,0,255} };
	COORD center = { 500.0,300.0 };
	double radius = 100;
	double radius2 = 180;
	rect* rect1 = new rect(hWnd, hDeviceContext, hPen, center, color1[2], { 0,360 }, { 360,0 });
	circle *circle1= new circle(hWnd, hDeviceContext, hPen, 800, 800, color1[0], center, radius2);
	ellipse *as = new ellipse(hWnd, hDeviceContext, hPen, 800, 800, color1[1], center, radius, radius2);

	shape* sd[3] = {rect1,circle1,as};
	switch (lpCmdLine)
	{
	case WM_KEYDOWN:
		DestroyWindow(hWnd);
		break;
	case WM_PAINT:
		hDeviceContext= BeginPaint(hWnd, &ps);
		circle1->setiStyle(PS_SOLID);
		circle1->setPaint(true);
		as->setiStyle(PS_SOLID);
		as->setPaint(true);

		for (int i = 0; i < 3; i++)
		{
			sd[i]->draw();
		}

		EndPaint(hWnd, &ps);
		delete rect1;
		delete circle1;
		delete as;
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		ReleaseDC(hWnd, hDeviceContext);
		DeleteObject(hPen);
		break;
	default:
		return DefWindowProc(hWnd, lpCmdLine, wparam, lparam);
	}
}

LRESULT InitWindow(HINSTANCE hInstance, int nCmdShow)
{
	WNDCLASSEX wnd;
	wnd.cbSize = sizeof(WNDCLASSEX);
	wnd.cbClsExtra = 0;
	wnd.cbWndExtra = 0;
	wnd.hCursor = NULL;
	wnd.hIcon = NULL;
	wnd.hIconSm = NULL;
	wnd.hbrBackground = (HBRUSH)COLOR_WINDOW;
	wnd.hInstance = hInstance;
	wnd.lpfnWndProc = WndProc;
	wnd.lpszClassName = "wnd";
	wnd.style = CS_HREDRAW | CS_VREDRAW;
	wnd.lpszMenuName = NULL;

	if (!RegisterClassEx(&wnd))
		return E_FAIL;

	hWnd = CreateWindow("wnd", "Urok_1", WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, CW_USEDEFAULT, window_size_x, windows_size_y, hWnd, NULL, hInstance, NULL);

	if (!hWnd)
		return E_FAIL;

	UpdateWindow(hWnd);
	ShowWindow(hWnd, nCmdShow);
}