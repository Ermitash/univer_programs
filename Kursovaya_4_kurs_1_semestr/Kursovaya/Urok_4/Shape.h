#pragma once
#include <iostream>
#include <Windows.h>
using namespace std;

struct color
{
	void set_color(int R, int G, int B)
	{
		this->R = R;
		this->G = G;
		this->B = B;
	}
	int R;
	int G;
	int B;
};

class shape
{
public:
	virtual void draw() = 0;
	void setColor(color color1);
	color getColor();
	void setCenter(COORD center);
	COORD getCenter();
	void setHWND(HWND hwnd);
	HWND getHWND();
	void setHDC(HDC hdc);
	HDC getHDC();
	void sethPen(HPEN hPen);
	HPEN gethPen();
	shape();
protected:
	HWND hwnd;
	HDC hdc;
	HPEN hPen;

	COORD center;
	color color_type;
};