#include "stdafx.h"
using namespace std;

void show(trans a[], int b, Average c, Queue d, Stack e)
{
	double itogo_LIFO = 0, itogo_FIFO = 0, itogo_average = 0;

	cout << "������������ ��: " << a[0].company->name << endl;
	cout << "��� ��:";
	cout.width(17);
	cout<< a[0].company->type << endl;
	cout << endl;
	cout << "��� ������ ���-��, ��.  ����, ���.  ����� LIFO  ����� FIFO  ����� ��. ����" << endl;
	cout << endl;
	
	for (int i = 0; i < b; i++)
	{
		itogo_LIFO += e.getLIFO()[i];
		itogo_FIFO += d.getFIFO()[i];
		itogo_average += c.getAverage_company()[i];

		char* type;
		if (a[i].type == 1)
			type = "�������";
		else if (a[i].type == 0)
			type = "�������";
		else
		{
			cout << "������� �� �� �����!" << endl;
			abort();
		}

		cout << type << setw(10)<< a[i].company->amount<< setw(13)<< a[i].company->price <<setw(13)<<e.getLIFO()[i]
			<<setw(13)<<d.getFIFO()[i]<<setw(13)<<c.getAverage_company()[i]<<endl;
	}
	cout << endl;
	cout << "�����:" << setw(37) << itogo_LIFO << setw(13) << itogo_FIFO << setw(13) << itogo_average << endl;
	
}

int main()
{
	setlocale(LC_ALL, "Russian");

	trans IrkutskEnergo[8] = {{ 1, "�������������","�����",1500,18.47f },
	                          { 1, "�������������","�����",1500,18.98f },
	                          { 1, "�������������","�����",1500,19.45f },
	                          { 1, "�������������","�����",1500,20.84f },
	                          { 1, "�������������","�����",1500,21.36f },
	                          { 0, "�������������","�����",1500,20.61f },
	                          { 0, "�������������","�����",1500,19.99f },
	                          { 0, "�������������","�����",1500,18.74f } };


	Stack Stack_Irkutsk(IrkutskEnergo, 8);
	Queue Queue_Irkutsk(IrkutskEnergo, 8);
	Average Average_Irkutsk(IrkutskEnergo, 8);

	Stack_Irkutsk.LIFO();
	Queue_Irkutsk.FIFO();
	Average_Irkutsk.average_price();
	show(IrkutskEnergo, 7, Average_Irkutsk, Queue_Irkutsk, Stack_Irkutsk);
	system("pause");
	return 0;
}