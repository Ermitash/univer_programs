#pragma once
#include "stdafx.h"

class Stack
{
public:
	Stack(trans a[], int b);
	double* getLIFO();
	void LIFO();
private:
	trans* Company;
	trans* begin;
	double* LIFO_company;
	int count_trans;
};