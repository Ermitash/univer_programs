#pragma once
#include <iostream>
#include <iomanip>
using namespace std;


class stock
{
	friend ostream &operator<<(ostream &s, const stock &a);
public:
	stock(char* name, char* type, int amount, float price);
	stock operator+(const stock &a);
	stock operator-(const stock &a);
    double return_cost_of_position();
public:
	char* name;
	char* type;
	int amount;
	float price;
	double cost_of_position;
};