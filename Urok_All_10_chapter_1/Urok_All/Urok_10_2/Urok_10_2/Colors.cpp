#include "Colors.h"

extern void colors(bool &stop)
{
	static int x = 0;
	static int y = 0;

	COORD xy = { x,y };

	HANDLE Hstdout = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleCursorPosition(Hstdout, xy);

	if (x < 39 && y < 12)
	{
		SetConsoleTextAttribute(Hstdout, BACKGROUND_BLUE);
	}
	else if (x >= 39 && y < 12)
	{
		SetConsoleTextAttribute(Hstdout, BACKGROUND_RED+BACKGROUND_INTENSITY);
	}
	else if (x < 39 && y >= 12)
	{
		SetConsoleTextAttribute(Hstdout, BACKGROUND_GREEN + BACKGROUND_INTENSITY);
	}
	else if (x >= 39 && y >= 12)
	{
		SetConsoleTextAttribute(Hstdout, BACKGROUND_GREEN);
	}

	cout << " ";
	if (x < 79)
	{
		x++;
	}
	else
	{
		x = 0;
		y++;
	}

	if (y>=25)
	{
		stop = false;
	}
	
}