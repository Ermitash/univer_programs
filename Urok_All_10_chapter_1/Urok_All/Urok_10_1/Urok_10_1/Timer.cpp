#include "Timer.h"

extern void print(time1 time)
{
	

	COORD center = { 39,12 };
	COORD begin = { 0,0 };
	static int schetchik = 0;
	static int background = 0;
	HANDLE Hstdout = GetStdHandle(STD_OUTPUT_HANDLE);

	switch (schetchik)
	{
	case 0:
		background = BACKGROUND_BLUE;
		break;
	case 1:
		background = BACKGROUND_GREEN;
		break;
	case 2:
		background = BACKGROUND_RED;
		break;
	case 3:
		background = BACKGROUND_BLUE + BACKGROUND_INTENSITY;
		break;
	case 4:
		background = BACKGROUND_GREEN+BACKGROUND_INTENSITY;
		break;
	case 5:
		background = BACKGROUND_RED + BACKGROUND_INTENSITY;
		break;
	case 6:
		background = BACKGROUND_BLUE + BACKGROUND_INTENSITY+BACKGROUND_INTENSITY;
		break;
	case 7:
		background = BACKGROUND_GREEN + BACKGROUND_INTENSITY+BACKGROUND_INTENSITY;
		break;
	case 8:
		background = BACKGROUND_RED + BACKGROUND_INTENSITY+BACKGROUND_INTENSITY;
		break;

	default:
		break;
	}

	SetConsoleTextAttribute(Hstdout, schetchik+background);
	system("cls");
	SetConsoleCursorPosition(Hstdout, center);
	

	cout.width(2);
	cout.fill('0');
	cout << time.hours << ":";
	cout.width(2);
	cout.fill('0');
	cout << time.minutes << ":";
	cout.width(2);
	cout.fill('0');
	cout<< time.seconds;
	SetConsoleCursorPosition(Hstdout, begin);

	schetchik++;
	if (schetchik == 9)
		schetchik = 0;
}

extern void time_infinite(time1 time)
{
	time.hours = 1;
	time.minutes = 14;
	time.seconds = 56;

	while (true)
	{
		print(time);
		Sleep(1000);
		time.seconds++;
		if (time.seconds == 60)
		{
			time.minutes++;
			time.seconds = 0;
		}
		if (time.minutes == 60)
		{
			time.hours++;
			time.minutes = 0;
		}
		if (time.hours == 24)
		{
			time.hours = 0;
		}
	}
}