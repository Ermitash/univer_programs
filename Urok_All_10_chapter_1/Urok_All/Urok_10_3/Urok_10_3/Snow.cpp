#include "snow.h"

extern void random(COORD snowflakes[], int number)
{
	srand(time(0));
	int randomer = 2356;
	int x = rand() % 80;
	int y = rand() % 25;
	for (int i = 0; i < number; i++)
	{
		snowflakes[i].X = x;
		snowflakes[i].Y = y;

		srand(randomer*time(0));
		x = rand() % 80;
		y = rand() % 25;
		randomer = (randomer + 1000)*(randomer - 500)*sin(randomer);
	}
}

extern void snow(COORD snowflakes[], int number)
{
	HANDLE Houtput = GetStdHandle(STD_OUTPUT_HANDLE);
	char snowflake = 15;

	SetConsoleTextAttribute(Houtput, 7+BACKGROUND_BLUE);
	system("cls");

	for (int i = 0; i < number; i++)
	{
		SetConsoleCursorPosition(Houtput, snowflakes[i]);
		cout << snowflake;
	}

	while (true)
	{
		Sleep(500);
		for (int i = 0; i < number; i++)
		{
			snowflakes[i].Y += 1;
			if (snowflakes[i].Y> 24)
			{
				snowflakes[i].Y = 0;
			}
		}

		system("cls");
		for (int i = 0; i < number; i++)
		{
			SetConsoleCursorPosition(Houtput, snowflakes[i]);
			cout << snowflake;
		}
	}
}