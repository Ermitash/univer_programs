#pragma once
#include <ctime>
#include <iostream>
#include <vector>
#include <string>
using namespace std;


class Date
{
private:
	tm date;
public:
	friend ostream& operator << (ostream& os, const Date& dt);
	friend istream& operator >> (istream& os, Date& dt);
};


struct names
{
	int indentificator;
	string bank_name;
};

struct plans
{
	int indentificator;
	string count_name;
};

struct money1
{
	Date date;
	double money;

};

struct res
{
	int regn;
	int num_sc;
	Date dt;
	double iitg;
};


void read_names  (vector<names>&name, char* file);
void read_plans  (vector<plans>&name, char* file);
void read_money  (vector<money1>&name, char* file);
string read_name(vector<names>name, int a);
string read_plan(vector<plans>name, int a);
void search_money(vector<money1>name_1 ,vector<res>&name_2);
void show_res    (vector<names>name, vector<plans>name_2, vector<res> name_3);
void write_res   (vector<res>&name, char* file);