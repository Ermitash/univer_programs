#include "Random_variants_for_students.h"

int main()
{
	setlocale(LC_ALL, "Russian");

	deque<person> names;
	vector<int> random_variants;
	map<person, int> list_with_students_and_variants;
	char* file = "group.txt", *file_1 = "group_1.txt";

	read_name(names, file);

	int kol_variantov;
	cout << "������� ���������� ���������: ";
	cin >> kol_variantov;

	random(random_variants, names.size(), kol_variantov);

	set_variants_in_maps(list_with_students_and_variants, random_variants, names);
	show_variants(list_with_students_and_variants);

	write_variants(list_with_students_and_variants, file_1);

	system("pause");
	return 0;
}