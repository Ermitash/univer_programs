#pragma once
#include <iostream>
#include <deque>
#include <vector>
#include <string>
#include <fstream>
#include <time.h>
#include <windows.h>
#include <map>
#include <iomanip>
#include <ios>
using namespace std;


class person
{
public:
	string second_name;
	string first_name;
	friend bool operator < (const person &v1, const person &v2);
};


void read_name(deque<person>&name, char* file);
void random(vector<int> &variants, int size_of_deque, int kol_vars);
void set_variants_in_maps(map<person, int> &list_with_students_and_variants, vector<int> &variants, deque<person>&name);
void show_variants(map<person, int> &list_with_students_and_variants);
void write_variants(map<person, int>& list_with_students_and_variants, char* file);