#include "Random_variants_for_students.h"

bool operator<(const person & v1, const person & v2)
{
	return (v1.second_name < v2.second_name) ? true : false;
}

void read_name(deque<person>& name, char * file)
{
	ifstream vvodim_vector(file, ios::in);

	person name_1;

	while (!vvodim_vector.eof())
	{
		vvodim_vector >> name_1.second_name>>name_1.first_name;
		name.push_back(name_1);
	}
}

void random(vector<int>& variants, int size_of_deque, int kol_vars)
{
	vector<int>::iterator it;

	if (kol_vars >= size_of_deque)
	{
		int schetchik = 0;

		while (schetchik<size_of_deque)
		{
			srand(time(NULL));
			int var = rand() % kol_vars + 1;

			it = find(variants.begin(), variants.end(), var);
			if (it!=variants.end())
			{
				continue;
			}
			else
			{
				schetchik++;
				variants.push_back(var);
			}
		}
	}
	else
	{
		int povtor = ceil( (double)size_of_deque / (double)kol_vars);

		int schetchik = 0;

		while (schetchik<size_of_deque)
		{
			Sleep(1000);
			srand(time(NULL));
			int var = rand() % kol_vars + 1;

			int kol= count(variants.begin(), variants.end(), var);
			
			if (kol == 0)
			{
				schetchik++;
				variants.push_back(var);
			}
			else
			{
				if (kol<=povtor)
				{
					schetchik++;
					variants.push_back(var);
				}
				else
				{
					continue;
				}
				
			}
		}
	}
}

void set_variants_in_maps(map<person, int>& list_with_students_and_variants, vector<int>& variants, deque<person>& name)
{
	for (int i = 0; i < name.size(); i++)
	{
		list_with_students_and_variants.insert(pair<person, int>(name[i], variants[i]));
	}
}

void show_variants(map<person, int>& list_with_students_and_variants)
{
	map<person, int>::iterator iter;

	for (iter = list_with_students_and_variants.begin(); iter != list_with_students_and_variants.end(); iter++)
	{
		cout << setw(15) << left<<(*iter).first.second_name << setw(15) << left << (*iter).first.first_name << setw(2) << left <<"� " << setw(3) << left<<(*iter).second << endl;
	}
}

void write_variants(map<person, int>& list_with_students_and_variants, char* file)
{
	map<person, int>::iterator iter;

	ofstream vivod(file, ios::out);

	for (iter = list_with_students_and_variants.begin(); iter != list_with_students_and_variants.end(); iter++)
	{
		vivod << setw(15) << left << (*iter).first.second_name << setw(15) << left << (*iter).first.first_name << setw(2) << left << "� " << setw(3) << left << (*iter).second << endl;
	}
}
