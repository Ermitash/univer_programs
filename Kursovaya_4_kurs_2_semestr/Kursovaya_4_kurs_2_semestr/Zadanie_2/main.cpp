#include <iostream>
#include <vector>
#include "Bank.h"
#include <algorithm>
using namespace std;

vector<names> names_1;
int main()
{
	char* names_2 = "NAMES.txt", *names_3 = "NAMES_sort.txt";

	read_names(names_1, names_2);
	sort(names_1.begin(), names_1.end(), [](const names& a, const names& b) {
		return a < b;});
	write_names(names_1, names_3);

	return 0;
}