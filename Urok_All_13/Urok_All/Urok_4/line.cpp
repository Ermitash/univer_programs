#include "line.h"

void line::paint_line()
{
	HDC hDeviceContext;
	HPEN hPen;

	hDeviceContext = GetDC(hWnd);
	hPen = CreatePen(iStyle, tolchina, color);
	SelectObject(hDeviceContext, hPen);

	MoveToEx(hDeviceContext, start.x, start.y, NULL);
	LineTo(hDeviceContext, end.x,end.y);

	ReleaseDC(hWnd, hDeviceContext);
	DeleteObject(hPen);
}

line::line(POINT start, POINT end, float tolchina, int iStyle, HWND hWnd, COLORREF color)
{
	line::start = start;
	line::end = end;
	line::iStyle = iStyle;
	line::hWnd = hWnd;
	line::color = color;
	line::tolchina = tolchina;
	paint_line();
}
