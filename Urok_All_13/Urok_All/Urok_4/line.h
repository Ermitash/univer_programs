#pragma once
#include <iostream>
#include <Windows.h>
using namespace std;
class line
{
public:
	line(POINT start, POINT end,float tolchina, int iStyle, HWND hWnd, COLORREF color);
	void paint_line();
private:
	POINT start;
	POINT end;
	int iStyle;
	HWND hWnd;
	COLORREF color;
	float tolchina;
};