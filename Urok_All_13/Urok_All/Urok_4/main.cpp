#include <iostream>
#include <Windows.h>
#include "line.h"
#include <conio.h>
using namespace std;

int main()
{
	HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
	CONSOLE_CURSOR_INFO curs;
	GetConsoleCursorInfo(hStdout, &curs);
	curs.bVisible = FALSE;
	SetConsoleCursorInfo(hStdout, &curs);
	HWND hWindow = NULL;

	int window_size_x = 700;
	int windows_size_y = 500;
	hWindow = GetConsoleWindow();
	SetWindowPos(hWindow, NULL, 1, 1, window_size_x, windows_size_y, NULL);

	while (true)
	{
		system("cls");
		POINT start[6] = { {250,50},{ 250,50 },{ 250,50 },{ 450,200 },{ 250,300 },{ 50,250 } };
		POINT end[6] = { {50,250},{ 250,300 } ,{ 450,200 },{ 250,300 },{ 50,250 },{ 450,200 } };
		COLORREF color = RGB(255, 0, 0);

		line* aa[6];

		for (int i = 0; i < 6; i++)
		{
			if(i!=5)
			aa[i] = new line(start[i], end[i], 2, PS_SOLID, hWindow, color);
			else
				aa[i] = new line(start[i], end[i], 1, PS_DASH, hWindow, color);
		}

		for (int i = 0; i < 6; i++)
		{
			delete aa[i];
		}
	}
	return 0;
}