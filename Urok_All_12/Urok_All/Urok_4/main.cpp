#include <Windows.h>
#include <iostream>
#include "circle.h"

HWND hWnd=NULL;
int window_size_x = 1366;
int windows_size_y = 768;
LRESULT CALLBACK WndProc(HWND hWnd,UINT lpCmdLine,WPARAM wparam,LPARAM lparam);
LRESULT InitWindow(HINSTANCE hInstance, int nCmdShow);
HINSTANCE hInstance_2 = NULL;

int WinMain(HINSTANCE hInstance, HINSTANCE PrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	hInstance_2 = hInstance;
	InitWindow( hInstance,nCmdShow);

	MSG msg = { 0 };

	while (msg.message!=WM_QUIT)
	{
		if (GetMessage(&msg, NULL,0, 0) != 0)
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	

	return (int)msg.wParam;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT lpCmdLine, WPARAM wparam, LPARAM lparam)
{
	PAINTSTRUCT ps;
	HDC hDeviceContext=NULL;
	HPEN hPen=NULL;
	COLORREF color[] = { RGB(255, 255, 255), RGB(0, 0, 0) };
	POINT center = { 500,500 };
	LOGFONT lf;
	HFONT hFont;
	int radius = 200;
	circle circle1(color[0],center, radius);

	switch (lpCmdLine)
	{
	case WM_PAINT:
	    hDeviceContext = BeginPaint(hWnd, &ps);
	
		circle1.draw_circle(hWnd, PS_SOLID, window_size_x, windows_size_y, true, hDeviceContext, hPen);
		circle1.set_circle(radius - radius * 0.1, center, color[1]);
		circle1.draw_circle(hWnd, PS_SOLID, window_size_x, windows_size_y, true, hDeviceContext, hPen);
		circle1.set_circle(radius - radius * 0.2, center, color[0]);
		circle1.draw_circle(hWnd, PS_SOLID, window_size_x, windows_size_y, true, hDeviceContext, hPen);
		circle1.set_circle(radius - radius * 0.3, center, color[1]);
		circle1.draw_circle(hWnd, PS_SOLID, window_size_x, windows_size_y, true, hDeviceContext, hPen);
		circle1.set_circle(radius - radius * 0.4, center, color[0]);
		circle1.draw_circle(hWnd, PS_SOLID, window_size_x, windows_size_y, true, hDeviceContext, hPen);
		circle1.set_circle(radius - radius * 0.5, center, color[1]);
		circle1.draw_circle(hWnd, PS_SOLID, window_size_x, windows_size_y, true, hDeviceContext, hPen);
		circle1.set_circle(radius - radius * 0.6, center, color[0]);
		circle1.draw_circle(hWnd, PS_SOLID, window_size_x, windows_size_y, true, hDeviceContext, hPen);
		circle1.set_circle(radius - radius * 0.7, center, color[1]);
		circle1.draw_circle(hWnd, PS_SOLID, window_size_x, windows_size_y, true, hDeviceContext, hPen);
		circle1.set_circle(radius - radius * 0.8, center, color[0]);
		circle1.draw_circle(hWnd, PS_SOLID, window_size_x, windows_size_y, true, hDeviceContext, hPen);
		circle1.set_circle(radius - radius * 0.9, center, color[1]);
		circle1.draw_circle(hWnd, PS_SOLID, window_size_x, windows_size_y, true, hDeviceContext, hPen);

		lf.lfHeight = 20;
		lf.lfItalic = 1;
		lf.lfStrikeOut = 0;
		lf.lfUnderline = 0;
		lf.lfWidth = 10;
		lf.lfWeight = 40;
		lf.lfCharSet = DEFAULT_CHARSET; //�������� �� ���������
		lf.lfPitchAndFamily = DEFAULT_PITCH; //�������� �� ���������
		lf.lfEscapement = 0;

		SetBkMode(hDeviceContext, TRANSPARENT);

		hFont = CreateFontIndirect(&lf);
		SelectObject(hDeviceContext, hFont);
		SetTextColor(hDeviceContext, RGB(255, 255, 255));
		TextOut(hDeviceContext, center.x - 175, center.y - 10, L"2    4     6     8     10     8     6     4     2", 50);
		TextOut(hDeviceContext, center.x - 5, center.y - 180, L"2", 1);
		TextOut(hDeviceContext, center.x - 5, center.y - 180 + 40, L"4", 1);
		TextOut(hDeviceContext, center.x - 5, center.y - 180 + 80, L"6", 1);
		TextOut(hDeviceContext, center.x - 5, center.y - 180 + 120, L"8", 1);
		TextOut(hDeviceContext, center.x - 5, center.y - 180 + 217, L"8", 1);
		TextOut(hDeviceContext, center.x - 5, center.y - 180 + 257, L"6", 1);
		TextOut(hDeviceContext, center.x - 5, center.y - 180 + 297, L"4", 1);
		TextOut(hDeviceContext, center.x - 5, center.y - 180 + 337, L"2", 1);
		
		ValidateRect(hWnd, NULL);
		EndPaint(hWnd, &ps);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		ReleaseDC(hWnd, hDeviceContext);
		DeleteObject(hPen);
		break;
	default:
		return DefWindowProc(hWnd, lpCmdLine, wparam, lparam);
	}
}

LRESULT InitWindow(HINSTANCE hInstance, int nCmdShow)
{
	WNDCLASSEX wnd;
	wnd.cbSize = sizeof(WNDCLASSEX);
	wnd.cbClsExtra = 0;
	wnd.cbWndExtra = 0;
	wnd.hCursor = NULL;
	wnd.hIcon = NULL;
	wnd.hIconSm = NULL;
	wnd.hbrBackground = (HBRUSH)COLOR_WINDOW;
	wnd.hInstance = hInstance;
	wnd.lpfnWndProc = WndProc;
	wnd.lpszClassName = L"wnd";
	wnd.style = CS_HREDRAW | CS_VREDRAW;
	wnd.lpszMenuName = NULL;

	if (!RegisterClassEx(&wnd))
		return E_FAIL;

	hWnd = CreateWindow(L"wnd", L"Urok_1", WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, CW_USEDEFAULT, window_size_x, windows_size_y, hWnd, NULL, hInstance, NULL);

	if (!hWnd)
		return E_FAIL;

	UpdateWindow(hWnd);
	ShowWindow(hWnd, nCmdShow);
}