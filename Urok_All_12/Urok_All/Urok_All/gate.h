#pragma once
#include<iostream>
using namespace std;

class gate
{
public:
	gate(int tarif) :payCars(0), nopayCars(0), totalCash(0),tarif(tarif)  {}
	void payingCars(gate*);
	void nopayingCars(gate*);
	void Show_for_a_day(gate*) const;
private:
	const int tarif;
	unsigned int payCars;
	unsigned int nopayCars;
	unsigned int totalCash;
};