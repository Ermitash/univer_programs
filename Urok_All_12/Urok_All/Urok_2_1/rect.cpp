#include "rect.h"

rect::rect()
{
	size_screen.X = GetSystemMetrics(SM_CXSCREEN);
	size_screen.Y = GetSystemMetrics(SM_CYSCREEN);

	left_bottom.X = 0;
	left_bottom.Y = size_screen.Y;

	right_top.X = size_screen.X;
	right_top.Y = 0;
}

void rect::set_rect(COORD left_bottom, COORD right_top, color color_type)
{
	rect::left_bottom = left_bottom;
	rect::right_top = right_top;
	rect::color_type = color_type;
}

void rect::draw_rect(HDC hdc)
{

	for (int i = right_top.Y; i <= left_bottom.Y; i++)
	{
		MoveToEx(hdc, left_bottom.X, i, NULL);
		LineTo(hdc, left_bottom.X + (right_top.X - left_bottom.X), i);

	}

}