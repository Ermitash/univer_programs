#include <Windows.h>
#include <iostream>
#include "rect.h"

HWND hWnd = NULL;
int window_size_x = 700;
int windows_size_y = 500;
LRESULT CALLBACK WndProc(HWND hWnd, UINT lpCmdLine, WPARAM wparam, LPARAM lparam);
LRESULT InitWindow(HINSTANCE hInstance, int nCmdShow);

int WinMain(HINSTANCE hInstance, HINSTANCE PrevInstance, LPSTR lpCmdLine, int nCmdShow)
{

	InitWindow(hInstance, nCmdShow);

	MSG msg = { 0 };

	while (msg.message != WM_QUIT)
	{
		if (GetMessage(&msg, NULL, 0, 0) != 0)
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}



	return (int)msg.wParam;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT lpCmdLine, WPARAM wparam, LPARAM lparam)
{
	COORD coord[4]   = { {0,  725},{ 100,600}, {350,600},{500,600} };
	COORD coord_2[4] = { {1367,0}, { 300,200 },{450,300},{700,100} };

	rect rectangle[4];
	color color[4];

	color[0].set_color(0, 0, 255);
	color[1].set_color(0, 255, 0);
	color[2].set_color(255, 0, 0);
	color[3].set_color(255, 255, 0);

	rectangle[0].set_rect(coord[0], coord_2[0], color[0]);
	rectangle[1].set_rect(coord[1], coord_2[1], color[1]);
	rectangle[2].set_rect(coord[2], coord_2[2], color[2]);
	rectangle[3].set_rect(coord[3], coord_2[3], color[3]);

	PAINTSTRUCT ps;
	HWND hwnd;
	hwnd = GetDesktopWindow();
	HDC hdc;
	hdc = GetWindowDC(hwnd);

	HPEN hPen = CreatePen(PS_SOLID, 0.9, RGB(color[0].R, color[0].G, color[0].B));
	SelectObject(hdc, hPen);
	
	switch (lpCmdLine)
	{
	case WM_KEYDOWN:
		DestroyWindow(hWnd);
	case WM_PAINT:
		//hdc = BeginPaint(hwnd, &ps);
		Sleep(1000);
		rectangle[0].draw_rect(hdc);
		Sleep(100);
		hPen = CreatePen(PS_SOLID, 0.9, RGB(color[1].R, color[1].G, color[1].B));
		SelectObject(hdc, hPen);
		Sleep(300);
		rectangle[1].draw_rect(hdc);
		Sleep(300);
		hPen = CreatePen(PS_SOLID, 0.9, RGB(color[2].R, color[2].G, color[2].B));
		SelectObject(hdc, hPen);
		rectangle[2].draw_rect(hdc);
		Sleep(300);
		hPen = CreatePen(PS_SOLID, 0.9, RGB(color[3].R, color[3].G, color[3].B));
		SelectObject(hdc, hPen);
		rectangle[3].draw_rect(hdc);
		//EndPaint(hwnd, &ps);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		ReleaseDC(hwnd, hdc);
		DeleteObject(hPen);
		break;
	default:
		return DefWindowProc(hWnd, lpCmdLine, wparam, lparam);
	}
}

LRESULT InitWindow(HINSTANCE hInstance, int nCmdShow)
{
	WNDCLASSEX wnd;
	wnd.cbSize = sizeof(WNDCLASSEX);
	wnd.cbClsExtra = 0;
	wnd.cbWndExtra = 0;
	wnd.hCursor = NULL;
	wnd.hIcon = NULL;
	wnd.hIconSm = NULL;
	wnd.hbrBackground = (HBRUSH)COLOR_WINDOW;
	wnd.hInstance = hInstance;
	wnd.lpfnWndProc = WndProc;
	wnd.lpszClassName = L"wnd";
	wnd.style = CS_HREDRAW | CS_VREDRAW;
	wnd.lpszMenuName = NULL;

	if (!RegisterClassEx(&wnd))
		return E_FAIL;

	hWnd = CreateWindow(L"wnd", L"Urok_1", WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, CW_USEDEFAULT, window_size_x, windows_size_y, hWnd, NULL, hInstance, NULL);

	if (!hWnd)
		return E_FAIL;

	ShowWindow(hWnd, nCmdShow);
}