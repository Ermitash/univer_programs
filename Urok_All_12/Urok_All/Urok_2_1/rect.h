#pragma once
#include <iostream>
#include <Windows.h>
using namespace std;

struct color
{
	void set_color(int R,int G,int B)
	{
	   this->R = R;
	   this->G = G;
	   this->B = B;
	}
	int R;
	int G;
	int B;
};

class rect
{
private:
	COORD size_screen;
	COORD left_bottom;
	COORD right_top;
	color color_type;
public:
	rect();
	void set_rect(COORD left_bottom, COORD right_top, color color_type);
	void draw_rect(HDC hdc);
};