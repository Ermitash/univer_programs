#include <Windows.h>
#include <iostream>
#include "rect.h"

HWND hWnd = NULL;
int window_size_x = 700;
int windows_size_y = 500;
LRESULT CALLBACK WndProc(HWND hWnd, UINT lpCmdLine, WPARAM wparam, LPARAM lparam);
LRESULT InitWindow(HINSTANCE hInstance, int nCmdShow);

int WinMain(HINSTANCE hInstance, HINSTANCE PrevInstance, LPSTR lpCmdLine, int nCmdShow)
{

	InitWindow(hInstance, nCmdShow);

	MSG msg = { 0 };

	while (msg.message != WM_QUIT)
	{
		if (GetMessage(&msg, NULL, 0, 0) != 0)
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}



	return (int)msg.wParam;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT lpCmdLine, WPARAM wparam, LPARAM lparam)
{
	color color;
	color.set_color(100, 100, 100);
	COORD height = { 0,153 };
	COORD width = { 273,0 };
	rect* fd;

	PAINTSTRUCT ps;
	HWND hwnd;
	hwnd = GetDesktopWindow();
	HDC hdc;
	hdc = GetWindowDC(hwnd);
	HPEN hPen=CreatePen(PS_SOLID, 0.9, RGB(color.R, color.G, color.B));
	
	

	switch (lpCmdLine)
	{
	case WM_KEYDOWN:
		DestroyWindow(hWnd);
	case WM_PAINT:
		 fd=new rect(hdc, hPen, height, width, color);
		 delete fd;
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		ReleaseDC(hwnd, hdc);
		DeleteObject(hPen);
		break;
	default:
		return DefWindowProc(hWnd, lpCmdLine, wparam, lparam);
	}
}

LRESULT InitWindow(HINSTANCE hInstance, int nCmdShow)
{
	WNDCLASSEX wnd;
	wnd.cbSize = sizeof(WNDCLASSEX);
	wnd.cbClsExtra = 0;
	wnd.cbWndExtra = 0;
	wnd.hCursor = NULL;
	wnd.hIcon = NULL;
	wnd.hIconSm = NULL;
	wnd.hbrBackground = (HBRUSH)COLOR_WINDOW;
	wnd.hInstance = hInstance;
	wnd.lpfnWndProc = WndProc;
	wnd.lpszClassName = "wnd";
	wnd.style = CS_HREDRAW | CS_VREDRAW;
	wnd.lpszMenuName = NULL;

	if (!RegisterClassEx(&wnd))
		return E_FAIL;

	hWnd = CreateWindow("wnd", "Urok_1", WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, CW_USEDEFAULT, window_size_x, windows_size_y, hWnd, NULL, hInstance, NULL);

	if (!hWnd)
		return E_FAIL;

	ShowWindow(hWnd, nCmdShow);
}