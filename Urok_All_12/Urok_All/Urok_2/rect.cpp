#include "rect.h"

void rect::set_rect(COORD left_bottom, COORD right_top, color color_type)
{
	rect::left_bottom = left_bottom;
	rect::right_top = right_top;
	rect::color_type = color_type;
}

void rect::draw_rect(HDC hdc)
{

	for (int i = right_top.Y; i <= left_bottom.Y; i++)
	{
		MoveToEx(hdc, left_bottom.X, i, NULL);
		LineTo(hdc, left_bottom.X + (right_top.X-left_bottom.X), i);

	}

}

rect::rect(HDC hdc, HPEN hPen,COORD height, COORD width, color color_type)
{
	size_screen.X = GetSystemMetrics(SM_CXSCREEN);
	size_screen.Y = GetSystemMetrics(SM_CYSCREEN);

	COORD left_b;
	COORD right_t;

	left_b = { 0 ,size_screen.Y };
	right_t = { size_screen.X, 0 };

	color fon = { 0,0,255 };
	set_rect(left_b, right_t, color_type);
    hPen = CreatePen(PS_SOLID, 0.9, RGB(fon.R, fon.G, fon.B));
	SelectObject(hdc, hPen);
	draw_rect(hdc);

	left_b.X = size_screen.X / 2 - width.X / 2;
	left_b.Y = size_screen.Y / 2 + height.Y / 2;
	right_t.X= size_screen.X / 2 + width.X / 2;
	right_t.Y = size_screen.Y / 2 - height.Y / 2;

	

	for (int i = 0; i < 5; i++)
	{
		if (i == 0)
		{
			Sleep(300);
			set_rect(left_b, right_t, color_type);
			hPen = CreatePen(PS_SOLID, 0.9, RGB(color_type.R, color_type.G, color_type.B));
			SelectObject(hdc, hPen);
			draw_rect(hdc);
		}
		else
		{
			Sleep(300);
			increase(&left_b, &right_t, &color_type);
			set_rect(left_b, right_t, color_type);
			 hPen = CreatePen(PS_SOLID, 0.9, RGB(color_type.R, color_type.G, color_type.B));
			SelectObject(hdc, hPen);
			draw_rect(hdc);
		}
	}

	for (int i = 5; i > 0; i--)
	{
		if (i == 5)
		{
			Sleep(300);
			set_rect(left_b, right_t, color_type);
			 hPen = CreatePen(PS_SOLID, 0.9, RGB(color_type.R, color_type.G, color_type.B));
			SelectObject(hdc, hPen);
			draw_rect(hdc);
		}
		else
		{
			Sleep(300);
			decrease(&left_b, &right_t, &color_type);
			set_rect(left_b, right_t, color_type);
		    hPen = CreatePen(PS_SOLID, 0.9, RGB(color_type.R, color_type.G, color_type.B));
			SelectObject(hdc, hPen);
			draw_rect(hdc);
		}
	}
}

COORD* rect::get_rect(COORD* left_bottom, COORD* right_top)
{
	COORD rect[2];
	rect[0] = *left_bottom;
	rect[1] = *right_top;
	return rect;
}

void rect::increase(COORD* left_b, COORD* right_t, color* color_type)
{
	left_b->X -= 126;
	left_b->Y += 77;
	right_t->X += 126;
	right_t->Y -=77;

	color_type->R += 100;
	color_type->G += 25;
	color_type->B += 25;
}

void rect::decrease(COORD* left_b, COORD* right_t, color* color_type)
{
	left_b->X += 126;
	left_b->Y -= 77;
	right_t->X -= 126;
	right_t->Y += 77;

	color_type->R -= 100;
	color_type->G -= 25;
	color_type->B -= 25;
}