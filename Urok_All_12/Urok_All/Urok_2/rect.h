#pragma once
#include <iostream>
#include <Windows.h>
using namespace std;

struct color
{
	void set_color(int R,int G,int B)
	{
	   this->R = R;
	   this->G = G;
	   this->B = B;
	}
	int R;
	int G;
	int B;
};

class rect
{
private:
	COORD size_screen;
	COORD left_bottom;
	COORD right_top;
	color color_type;
public:
	void increase(COORD* left_b, COORD* right_t, color* color_type);
	void decrease(COORD* left_b, COORD* right_t, color* color_type);
	rect(HDC hdc, HPEN hPen,COORD height = { 0,153 }, COORD width = { 273,0 }, color color_type = {50,50,50});
	COORD* get_rect(COORD* left_bottom, COORD* right_top);
	void set_rect(COORD left_bottom, COORD right_top, color color_type);
	void draw_rect(HDC hdc);
};