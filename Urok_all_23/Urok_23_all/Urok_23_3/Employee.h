#pragma once
#include <string>
#include <fstream>
#include <iterator>
#include <deque>
using namespace std;

struct employee
{
	int number;
	string second_name;
	string first_name;
	double count;

	friend ostream& operator <<(ostream &a, const employee &b)
	{
		a << b.number << " " << b.second_name << " " << b.first_name << " " << b.count;
		return a;
	}

	friend istream& operator >>(istream &a, employee &b)
	{
		a >> b.number >> b.second_name >> b.first_name  >> b.count;
		return a;
	}
};


void out_from_file(deque<employee> &a, char *name)
{
	ifstream vvod_potok(name, ios::in);
	istream_iterator<employee> begin_of_stream(vvod_potok);
	istream_iterator<employee> end_of_stream;

	copy(begin_of_stream, end_of_stream, back_inserter(a));
}


void show_deque(deque<employee> a)
{
	ostream_iterator<employee> show(cout, "\n");
	
	copy(a.begin(), a.end(), show);

}