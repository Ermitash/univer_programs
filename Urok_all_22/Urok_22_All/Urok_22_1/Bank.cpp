#include "Bank.h"
#include <fstream>
#include <string>
#include <iomanip>

ostream & operator<<(ostream & os, const Date & dt)
{
	// TODO: �������� ����� �������� return
	os << setw(2) << setfill('0') << dt.date.tm_mday << ":" << setw(2) << setfill('0')<< dt.date.tm_mon << ":" << dt.date.tm_year;
	cout<<setfill(' ');
	return os;
}


istream & operator >> (istream & os,  Date & dt)
{
	string date;
	string vremenno;
	os >> date;
	int schetchik = 0;

	for (int var=0;var<=date.length();var++)
	{
		if ((date[var] == '.')&& (schetchik==0))
		{
			dt.date.tm_mday = atoi(vremenno.c_str());
			vremenno = "";
			schetchik++;
		}
		else if ((date[var] == '.')&&( schetchik == 1))
		{
			dt.date.tm_mon = atoi(vremenno.c_str());
			vremenno = "";
			schetchik++;
		}
		else if ((date[var] == '\0')&&( schetchik >1))
		{
			dt.date.tm_year = atoi(vremenno.c_str());
			vremenno = "";
			schetchik++;
			break;
		}
		else
		{
			vremenno += date[var];
		}

	}
	// TODO: �������� ����� �������� return
	return os;
}

void read_names(vector<names>& name, char * file)
{
	ifstream vvodim_vector(file, ios::in);

	int indentificator=0;
	string bank_name;

	while (!vvodim_vector.eof())
	{
		vvodim_vector >> indentificator;
		getline(vvodim_vector, bank_name, '\n');
		names n;
		n.indentificator = indentificator;
		n.bank_name = bank_name.substr(1, bank_name.length() - 1);
		name.push_back(n);
	}
}

void read_plans(vector<plans>& name, char * file)
{
	ifstream vvodim_vector(file, ios::in);
	vvodim_vector.exceptions(std::ifstream::failbit | std::ifstream::badbit);
	int indentificator = 0;
	string count_name;

	while (!vvodim_vector.eof())
	{
		try
		{
			vvodim_vector >> indentificator;
			getline(vvodim_vector, count_name, '\n');
		}
		catch (const std::ios_base::failure & e)
		{
			break;
		}
		
		plans n;
		n.indentificator = indentificator;
		n.count_name = count_name.substr(1, count_name.length()-1);
		name.push_back(n);
	}
}

void read_money(vector<money1>& name, char * file)
{
	ifstream vvodim_vector(file, ios::in);

	double indentificator = 0;
	Date d;

	while (!vvodim_vector.eof())
	{
		
		vvodim_vector >>d>>indentificator;

		money1 n;
		n.date = d;
		n.money = indentificator;
		name.push_back(n);
	}
}

string read_name(vector<names> name, int a)
{
	for (int i = 0; i < name.size(); i++)
	{
		if (name[i].indentificator == a)
			return name[i].bank_name;
	}
}

string read_plan(vector<plans> name, int a)
{
	for (int i = 0; i < name.size(); i++)
	{
		if (name[i].indentificator == a)
			return name[i].count_name;
	}
}

void search_money(vector<money1>name_1,  vector<res>&name_2)
{
	int i, j;
	cout << "������� ��������������� ����� �����: ";
	cin >> i;
	i--;
	cout << "������� ����� �����: ";
	cin >> j;
	j--;

	res a;
	for (int i = 0; i < name_1.size() - 1; i++)
	{
		a.regn = i;
		a.num_sc = j;
		a.dt = name_1[i].date;
		a.iitg = name_1[i].money;
		name_2.push_back(a);
	}
	

}

void show_res(vector<names>name, vector<plans>name_2, vector<res> name_3)
{
	cout << "�������� ������� �� ����� �" <<name_2[name_3[0].num_sc].indentificator <<" "<< name_2[name_3[0].num_sc].count_name << endl;
	cout << "��� ����� �" << name[name_3[0].num_sc].indentificator << " " << name[name_3[0].num_sc].bank_name << endl;
	cout << endl << endl << endl;

	cout << "        ����" << "                             �����" << endl;
	for (int i = 0; i < name_3.size(); i++)
	{
		cout <<"      "<< name_3[i].dt <<setw(30) << name_3[i].iitg<<"�" << endl;
	}
}

void write_res(vector<res>& name, char *file)
{
	ofstream vivod(file, ios::out);

	vivod << "  REGN NUM_SC      DT          IITG" << endl;
	for (int i = 0; i < name.size(); i++)
	{
		vivod <<"    "<< name[i].regn <<"    "<< name[i].num_sc+1 << "      "<< name[i].dt << "    "<< name[i].iitg << endl;
	}

}
