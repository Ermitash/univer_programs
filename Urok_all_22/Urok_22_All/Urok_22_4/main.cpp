#include <deque>
#include <string>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <iterator>

using namespace std;

struct employees
{
	int number;
	string first_name;
	string second_name;
	double amount;
};

void read_employees(deque<employees> &emp, char* file)
{
	ifstream in(file, ios::in);
	employees object;
	while (!in.eof())
	{
		in >> object.number>>object.second_name>>object.first_name>>object.amount;
		emp.push_back(object);
	}
}

void show_deque(deque<employees>emp)
{
	for (int i = 0; i < emp.size(); i++)
	{
		cout << emp[i].number << " " << emp[i].second_name << " " << emp[i].first_name << " " << emp[i].amount<<endl;
	}
}

void export_deque(deque<employees> &emp, char* file)
{
	ofstream out(file, ios::out);
	employees object;
	
	for (int i = 0; i < emp.size(); i++)
	{
		out << emp[i].number << " " << emp[i].second_name << " " << emp[i].first_name << " " << emp[i].amount<<endl;
	}
}

int main()
{
	setlocale(LC_ALL, "Russian");

	deque<employees> first;
	deque<employees> second;
	deque<employees> all;
	char* file_1 = "list.txt", *file_2 = "list_1.txt";

	read_employees(first, file_1);
	show_deque(first);
	cout << endl;
	read_employees(second, file_2);
	show_deque(second);
	cout << endl;

	all.insert(all.end(), first.begin(), first.end());
	all.insert(all.end(), second.begin(), second.end());
	show_deque(all);
	export_deque(all, "list_3.txt");


	system("pause");
}