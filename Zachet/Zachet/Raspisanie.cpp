#include "Raspisanie.h"

void output_station(char** nazvanie, const int kol_stanciy)
{
	cout << "����������� �����������:" << endl << endl;
	for (int i = 0; i < kol_stanciy;i++)
	{
		cout << i+1 << "-" << nazvanie[i] << endl;
	}
}

void output_raspisanie(int **raspisanie)
{
	cout << setw(50) << "���������� ����������" << endl << endl;
	cout << setw(43) << "�������" << endl;

	for (int i = 0; i < 6; i++)
	{
		if (i == 0)
		{
			cout << setw(29) << "�" << i + 1 << setw(3);
		}
		else
		{
			cout<< "�" << i + 1 << setw(3);
		}
	}

	cout << endl;

	int k = 1;
	for (int i = 0; i < 4; i++)
	{
		cout << "������������ � " << i + 1 << ":";
		for (int j = 0; j < 6; j++)
		{
			
			if (raspisanie[i][k] == j + 1)
			{
				if (j == 0)
				{
					cout<<setw(13)<< raspisanie[i][k + 1];
					k = k + 2;
				}
				else
				{
					cout << setw(4) << raspisanie[i][k + 1];
					k = k + 2;
				}
			}
			else
			{
				if (j == 0)
				{
					cout << setw(13)<<"  ";
				}
				else
				{
					cout << setw(4)<<"  ";
				}
			}
		}
		k = 1;
		cout << endl;
	}
}

int input_station()
{
	bool proverka = true;
	int input;
	cout << "������� ����� ������ ��� ��� �������:";
	cin >> input;
	while (proverka==true)
	{
		if (input > 6 || input < 1)
		{
			cout << "�� ����� ������������ �����!" << endl;
			cout << "������� ����� ������ ��� ��� �������:";
			cin >> input;
		}
		else
			proverka = false;
	}
	return input;
}

void podbor(int** raspisanie, int* kol_el, int number, char**nazvanie)
{
	int time[4] = { 100,100,100,100 };
	int n = 0;
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < kol_el[i]; j=j+2)
		{
			if (raspisanie[i][j] == number)
			{
				cout << "������������ � " << i + 1 << " ����� � ���� "<<raspisanie[i][j+1]<<endl;
				time[i] = raspisanie[i][j + 1];
				n++;
			}
		}
	}
	cout << endl;
	cout << "���������� ��������� ���������: " << n << endl;
	cout << endl;
	int sravnenie=time[0];
	for (int i = 0; i < 4; i++)
	{

		if (time[i] < sravnenie)
		{
			sravnenie = time[i];
		}
	}
	cout << "����������� ���������� ������� �� ������� " << nazvanie[number-1] << " ����� " << sravnenie << endl;
	cout << endl;
	char symbol;
	cout << "������� ������ �������? y-��, n-���";
	cin >> symbol;

	bool proverka=true;
	while (proverka == true)
	{
		if (symbol == 'y')
		{
			proverka = false;
			podbor(raspisanie, kol_el, input_station(), nazvanie);
		}
		else if (symbol == 'n')
		{
			proverka = false;
		}
		else
		{
			cout << "�� ����� �� �� �����!������� ������ �����!������� �����: ";
			cin >> symbol;
		}
	}
}