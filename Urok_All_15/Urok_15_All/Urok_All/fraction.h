#pragma once
#include <iostream>
#include <string>
using namespace std;

class fraction {
	friend ostream &operator<<(ostream &s, const fraction &one);
	friend istream &operator>>(istream &s, fraction &one);
public:
	fraction();
	fraction(int P, unsigned int Q);
	fraction operator+(const fraction &one);
	fraction operator-(const fraction &one);
	fraction operator*(const fraction &one);
	fraction operator/(const fraction &one);
	void sokrachenie(fraction &this_fraction);
private:
	int P;
    int Q;
};