#include "stock.h"

stock::stock(char * name, char * type, int amount, float price)
{
	stock::name = name;
	stock::type = type;
	stock::amount = amount;
	stock::price = price;
	cost_of_position = price*amount;
}

stock stock::operator+(const stock & a)
{
	stock vremenno(this->name, this->type, this->amount, this->price);
	vremenno.amount += a.amount;
	vremenno.cost_of_position = (vremenno.cost_of_position + a.cost_of_position);
	vremenno.price = vremenno.cost_of_position / vremenno.amount;
	return vremenno;
}

stock stock::operator-(const stock & a)
{
	stock vremenno(this->name, this->type, this->amount, this->price);
	vremenno.amount -= a.amount;
	vremenno.cost_of_position = vremenno.amount*vremenno.price;
	return vremenno;
}

double stock::return_cost_of_position()
{
	return cost_of_position;
}

ostream & operator<<(ostream &s, const stock & a)
{
	s << setw(20) << std::left << a.name << setw(10) << a.type << setw(14) << a.amount << setw(10) << a.price << setw(15) << a.cost_of_position;
	return s;
	// TODO: �������� ����� �������� return
}
