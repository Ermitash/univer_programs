#include "obiem.h"

void random(double mass[][3][3], int a)
{
	random_device rd;   // non-deterministic generator  
	mt19937 gen(rd());  // to seed mersenne twister.  
	uniform_real_distribution<> dist(1,999); // distribute results between 1 and 6 inclusive.  

	for (int i = 0; i < a; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			mass[2][i][j] = dist(gen);
		}
	}
}

void output(int a, double mass[][3][3],int mass1)
{
	cout << setw(30) << "������ �" << a << endl;
	cout << "�����" << setw(17) << "������" << setw(16) << "�������" << setw(15) << "����" << endl << endl;

	for(int i=0;i<mass1;i++)
	{
		cout << "����� �" << i + 1<<setw(15);
		for(int j=0;j<3;j++)
		{
			if (j == 2)
			{
				cout << mass[2][i][j];
			}
			else
			cout << mass[2][i][j] << setw(15);
		}
		cout << endl;
	}
}

void prisvoit_ukaz(double ***ukaz, double mass[3][3][3])
{
	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			for (int k = 0; k < 3; k++)
			{
				ukaz[i][j][k] == mass[i][j][k];
			}
		}
	}
}

void output(int a, double ***mass, int mass1)
{
	cout << setw(30) << "������ �" << a << endl;
	cout << "�����" << setw(17) << "������" << setw(16) << "�������" << setw(15) << "����" << endl << endl;

	for (int i = 0; i<mass1; i++)
	{
		cout << "����� �" << i + 1 << setw(15);
		for (int j = 0; j<3; j++)
		{
			if (j == 2)
			{
				cout << mass[2][i][j];
			}
			else
				cout << mass[2][i][j] << setw(15);
		}
		cout << endl;
	}
}