#include "Colors.h"

extern void colors(bool &stop)
{
	static int x = 0;
	static int y = 0;

	COORD xy = { x,y };

	HANDLE Hstdout = GetStdHandle(STD_OUTPUT_HANDLE);
	CONSOLE_CURSOR_INFO cci;
	cci.dwSize = 99;
	cci.bVisible = false;
	SetConsoleCursorInfo(Hstdout, &cci);
	SetConsoleCursorPosition(Hstdout, xy);

	if (x < 39 && y < 12)
	{
		SetConsoleTextAttribute(Hstdout, BACKGROUND_BLUE);
	}
	else if (x >= 39 && y < 12)
	{
		SetConsoleTextAttribute(Hstdout, BACKGROUND_RED+BACKGROUND_INTENSITY);
	}
	else if (x < 39 && y >= 12)
	{
		SetConsoleTextAttribute(Hstdout, BACKGROUND_GREEN + BACKGROUND_INTENSITY);
	}
	else if (x >= 39 && y >= 12)
	{
		SetConsoleTextAttribute(Hstdout, BACKGROUND_GREEN);
	}

	cout << " ";
	if (x < 79)
	{
			x++;
	}
	else
	{
		if (y >= 24)
		{
			stop = false;
			xy.X = 0;
			xy.Y = 0;
			SetConsoleCursorPosition(Hstdout, xy);
		}
		else
		{
			x = 0;
			y++;
		}
	}

}

void paint(rect a, char* text)
{
	HANDLE Hstdout = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(Hstdout, *text);
	CONSOLE_CURSOR_INFO cci;
	cci.dwSize = 99;
	cci.bVisible = false;
	SetConsoleCursorInfo(Hstdout, &cci);

	for (int i = a.xy1.Y; i <= a.xy2.Y; i++)
	{
		for (int j = a.xy1.X; j <= a.xy2.X; j++)
		{
			COORD xy = { j,i};
			SetConsoleCursorPosition(Hstdout, xy);
			cout << " ";
		}
	}
	SetConsoleCursorPosition(Hstdout, {0,0});
}

void change_colors(char *a,int b)
{
	char vremenno;
	char vremenno1;
	for (int i = 0; i < b; i++)
	{
		if (i == 0)
		{
			vremenno = a[i];
			a[i] = a[3];
		}
		else
		{
			vremenno1 = a[i];
			a[i] = vremenno;
			vremenno = vremenno1;
		}
	}
}