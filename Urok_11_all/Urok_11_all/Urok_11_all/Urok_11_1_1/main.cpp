#include <iostream>
#include "swap.h"
using namespace std;

int main()
{
	setlocale(LC_ALL, "Russian");

	int* x =new int(5);
	int* y = new int(10);

	cout << "������� swap �� ������: &� = " << x << " &y = " << y << endl << endl;
	cout << "������� swap �� ������: � = " << *x << " y = " << *y << endl << endl;

	swap(x, y);

	cout << "������� swap ����� ������: &� = " << x << " &y = " << y << endl << endl;
	cout << "������� swap ����� ������: � = " << *x << " y = " << *y << endl << endl;

	system("pause");
	return 0;
}