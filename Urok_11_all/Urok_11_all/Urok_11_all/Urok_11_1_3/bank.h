#include <iostream>
#include <iomanip>
using namespace std;

struct stock
{
	char* name, *type;
	int count;
	float cost;
	double cost_of_position;
};

struct sec
{
	stock state;
	stock buy;
	stock sell;
};

extern stock initialization(stock &, char*, char*, int, float);
extern double full_cost;
extern void cout_stock(stock);
extern sec sdelka(sec*);