#include "Header.h"

void print(int mass[4][10])
{
	for (int i=0;i<4;i++)
	{
		for (int j=0;j<10;j++)
		{
			cout << mass[i][j] << " ";
		}
		cout << endl;
	}
}

void print(int **a, int mass[4][10])
{
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < mass[i][0]; j++)
		{
			cout << a[i][j]<<" ";
		}
		cout << endl;
	}
}

void ukaz_initialization(int **a, int b[4][10])
{

	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < b[i][0]; j++)
		{
			a[i][j] = 1;
		}
	}
}