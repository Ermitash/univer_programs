#include "obiem.h"

void random(double **mass, int a)
{
	random_device rd;   // non-deterministic generator  
	mt19937 gen(rd());  // to seed mersenne twister.  
	uniform_real_distribution<> dist(1,999); // distribute results between 1 and 6 inclusive.  

	for (int i = 0; i < a; i++)
	{
		for (int j = 0; j < a; j++)
		{
			mass[i][j] = dist(gen);
		}
	}
}

void output(int a, double** mass,int mass1)
{
	cout << setw(30) << "������ �" << a << endl;
	cout << "�����" << setw(17) << "������" << setw(16) << "�������" << setw(15) << "����" << endl << endl;

	for(int i=0;i<mass1;i++)
	{
		cout << "����� �" << i + 1<<setw(15);
		for(int j=0;j<mass1;j++)
		{
			if (j == 2)
			{
				cout << mass[i][j];
			}
			else
			cout << mass[i][j] << setw(15);
		}
		cout << endl;
	}
}