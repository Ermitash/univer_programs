#include "ukazatel.h"

int factorial(int a)
{
	int factorial=1;
	for (int i = 0; i < a; i++)
	{
		factorial = factorial*(i + 1);
	}
	return factorial;
}

int summa(int a)
{
	int summa = 0;
	for (int i = 0; i < a; i++)
	{
		summa = summa+(i + 1);
	}
	return summa;
}

int kol_del_na_3(int a)
{
	int del_na_3 = 0;
	for (int i = 1; i <= a; i++)
	{
		if (i % 3 == 0)
			del_na_3++;
	}
	return del_na_3;
}
