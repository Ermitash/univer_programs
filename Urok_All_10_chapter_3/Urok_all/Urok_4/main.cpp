#include <Windows.h>
#include <iostream>
#include "circle.h"

HWND hWnd=NULL;
int window_size_x = 700;
int windows_size_y = 500;
LRESULT CALLBACK WndProc(HWND hWnd,UINT lpCmdLine,WPARAM wparam,LPARAM lparam);
LRESULT InitWindow(HINSTANCE hInstance, int nCmdShow);
HINSTANCE hInstance_2 = NULL;

int WinMain(HINSTANCE hInstance, HINSTANCE PrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	hInstance_2 = hInstance;
	InitWindow( hInstance,nCmdShow);

	MSG msg = { 0 };

	while (msg.message!=WM_QUIT)
	{
		if (GetMessage(&msg, NULL,0, 0) != 0)
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	

	return (int)msg.wParam;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT lpCmdLine, WPARAM wparam, LPARAM lparam)
{
	PAINTSTRUCT ps;
	HDC hDeviceContext=NULL;
	HPEN hPen=NULL;
	COLORREF color[] = { RGB(255, 255, 255), RGB(0, 0, 0) };
	POINT center = { 250,245 };
	LOGFONT lf;
	HFONT hFont;

	switch (lpCmdLine)
	{
	case WM_PAINT:
	    hDeviceContext = BeginPaint(hWnd, &ps);
		
		paint_circle(hWnd, color[0], center, 200, PS_SOLID, window_size_x, windows_size_y, true, hDeviceContext, hPen);
		paint_circle(hWnd, color[1], center, 200 - 200 * 0.1, PS_SOLID, window_size_x, windows_size_y, true,hDeviceContext,hPen);
		paint_circle(hWnd, color[0], center, 200 - 200 * 0.2, PS_SOLID, window_size_x, windows_size_y, true, hDeviceContext, hPen);
		paint_circle(hWnd, color[1], center, 200 - 200 * 0.3, PS_SOLID, window_size_x, windows_size_y, true, hDeviceContext, hPen);
		paint_circle(hWnd, color[0], center, 200 - 200 * 0.4, PS_SOLID, window_size_x, windows_size_y, true, hDeviceContext, hPen);
		paint_circle(hWnd, color[1], center, 200 - 200 * 0.5, PS_SOLID, window_size_x, windows_size_y, true, hDeviceContext, hPen);
		paint_circle(hWnd, color[0], center, 200 - 200 * 0.6, PS_SOLID, window_size_x, windows_size_y, true, hDeviceContext, hPen);
		paint_circle(hWnd, color[1], center, 200 - 200 * 0.7, PS_SOLID, window_size_x, windows_size_y, true, hDeviceContext, hPen);
		paint_circle(hWnd, color[0], center, 200 - 200 * 0.8, PS_SOLID, window_size_x, windows_size_y, true, hDeviceContext, hPen);
		paint_circle(hWnd, color[1], center, 200 - 200 * 0.9, PS_SOLID, window_size_x, windows_size_y, true, hDeviceContext, hPen);
		
		lf.lfHeight = 20;
		lf.lfItalic = 1;
		lf.lfStrikeOut = 0;
		lf.lfUnderline = 0;
		lf.lfWidth = 10;
		lf.lfWeight = 40;
		lf.lfCharSet = DEFAULT_CHARSET; //�������� �� ���������
		lf.lfPitchAndFamily = DEFAULT_PITCH; //�������� �� ���������
		lf.lfEscapement = 0;

		SetBkMode(hDeviceContext, TRANSPARENT);

		hFont = CreateFontIndirect(&lf);
		SelectObject(hDeviceContext, hFont);
		SetTextColor(hDeviceContext, RGB(255, 255, 255));
		TextOut(hDeviceContext, 75, 235, L"2    4     6     8     10     8     6     4     2", 50);
		TextOut(hDeviceContext, 245, 68, L"2", 1);
		TextOut(hDeviceContext, 245, 108, L"4", 1);
		TextOut(hDeviceContext, 245, 148, L"6", 1);
		TextOut(hDeviceContext, 245, 188, L"8", 1);
		TextOut(hDeviceContext, 245, 285, L"8", 1);
		TextOut(hDeviceContext, 245, 325, L"6", 1);
		TextOut(hDeviceContext, 245, 365, L"4", 1);
		TextOut(hDeviceContext, 245, 405, L"2", 1);
		
		ValidateRect(hWnd, NULL);
		EndPaint(hWnd, &ps);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		ReleaseDC(hWnd, hDeviceContext);
		DeleteObject(hPen);
		break;
	default:
		return DefWindowProc(hWnd, lpCmdLine, wparam, lparam);
	}
}

LRESULT InitWindow(HINSTANCE hInstance, int nCmdShow)
{
	WNDCLASSEX wnd;
	wnd.cbSize = sizeof(WNDCLASSEX);
	wnd.cbClsExtra = 0;
	wnd.cbWndExtra = 0;
	wnd.hCursor = NULL;
	wnd.hIcon = NULL;
	wnd.hIconSm = NULL;
	wnd.hbrBackground = (HBRUSH)COLOR_WINDOW;
	wnd.hInstance = hInstance;
	wnd.lpfnWndProc = WndProc;
	wnd.lpszClassName = L"wnd";
	wnd.style = CS_HREDRAW | CS_VREDRAW;
	wnd.lpszMenuName = NULL;

	if (!RegisterClassEx(&wnd))
		return E_FAIL;

	hWnd = CreateWindow(L"wnd", L"Urok_1", WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, CW_USEDEFAULT, window_size_x, windows_size_y, hWnd, NULL, hInstance, NULL);

	if (!hWnd)
		return E_FAIL;

	UpdateWindow(hWnd);
	ShowWindow(hWnd, nCmdShow);
}