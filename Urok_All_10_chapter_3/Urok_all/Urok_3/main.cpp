#include <Windows.h>
#include <iostream>
#include "parallelogram.h"

HWND hWnd=NULL;
int window_size_x = 700;
int windows_size_y = 500;
LRESULT CALLBACK WndProc(HWND hWnd,UINT lpCmdLine,WPARAM wparam,LPARAM lparam);
LRESULT InitWindow(HINSTANCE hInstance, int nCmdShow);

int WinMain(HINSTANCE hInstance, HINSTANCE PrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	
	InitWindow( hInstance,nCmdShow);

	MSG msg = { 0 };

	while (msg.message!=WM_QUIT)
	{
		if (GetMessage(&msg, NULL,0, 0) != 0)
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	

	return (int)msg.wParam;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT lpCmdLine, WPARAM wparam, LPARAM lparam)
{
	PAINTSTRUCT ps;
	HDC hDeviceContext=NULL;
	HPEN hPen=NULL;
	COLORREF color[] = { RGB(255, 0, 0), RGB(255, 255, 0), RGB(0, 255, 0), RGB(155, 50, 0) };
	POINT parallelogram[] = { { 100,150 },{ 250,50 },{ 400,150 },{ 250,250 } };
	POINT parallelogram_1[] = { { 250,250 },{ 400,150 },{ 400,300 },{ 250,400 } };
	POINT parallelogram_2[] = { { 100,150 },{ 250,250 },{ 250,400 },{ 100,300 } };
	switch (lpCmdLine)
	{
	case WM_PAINT:
	    hDeviceContext = BeginPaint(hWnd, &ps);
		paint_parallelogram(hWnd, color[0], parallelogram, 4, PS_SOLID, window_size_x, windows_size_y, true,hDeviceContext,hPen);
		paint_parallelogram(hWnd, color[1], parallelogram_1, 4, PS_SOLID, window_size_x, windows_size_y, true, hDeviceContext, hPen);
		paint_parallelogram(hWnd, color[2], parallelogram_2, 4, PS_SOLID, window_size_x, windows_size_y, true, hDeviceContext, hPen);
		ValidateRect(hWnd, NULL);
		EndPaint(hWnd, &ps);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		ReleaseDC(hWnd, hDeviceContext);
		DeleteObject(hPen);
		break;
	default:
		return DefWindowProc(hWnd, lpCmdLine, wparam, lparam);
	}
}

LRESULT InitWindow(HINSTANCE hInstance, int nCmdShow)
{
	WNDCLASSEX wnd;
	wnd.cbSize = sizeof(WNDCLASSEX);
	wnd.cbClsExtra = 0;
	wnd.cbWndExtra = 0;
	wnd.hCursor = NULL;
	wnd.hIcon = NULL;
	wnd.hIconSm = NULL;
	wnd.hbrBackground = (HBRUSH)COLOR_WINDOW;
	wnd.hInstance = hInstance;
	wnd.lpfnWndProc = WndProc;
	wnd.lpszClassName = L"wnd";
	wnd.style = CS_HREDRAW | CS_VREDRAW;
	wnd.lpszMenuName = NULL;

	if (!RegisterClassEx(&wnd))
		return E_FAIL;

	hWnd = CreateWindow(L"wnd", L"Urok_1", WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, CW_USEDEFAULT, window_size_x, windows_size_y, hWnd, NULL, hInstance, NULL);

	if (!hWnd)
		return E_FAIL;

	UpdateWindow(hWnd);
	ShowWindow(hWnd, nCmdShow);
}