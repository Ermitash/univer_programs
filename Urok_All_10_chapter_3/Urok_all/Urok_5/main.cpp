#include <Windows.h>
#include <iostream>

HWND hWnd=NULL;
int window_size_x = 700;
int windows_size_y = 500;
LRESULT CALLBACK WndProc(HWND hWnd,UINT lpCmdLine,WPARAM wparam,LPARAM lparam);
LRESULT InitWindow(HINSTANCE hInstance, int nCmdShow);
void CreateButton(HWND hWnd, int position_x, int position_y, LPCWSTR text, int idenitify);

int WinMain(HINSTANCE hInstance, HINSTANCE PrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	
	InitWindow( hInstance,nCmdShow);

	MSG msg = { 0 };

	while (msg.message!=WM_QUIT)
	{
		if (GetMessage(&msg, NULL,0, 0) != 0)
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	

	return (int)msg.wParam;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT lpCmdLine, WPARAM wparam, LPARAM lparam)
{
	int wmId;
	int window_message;
	switch (lpCmdLine)
	{
	case WM_COMMAND:
		wmId = LOWORD(wparam);
		switch (wmId)
		{
		case 1000:
			window_message=MessageBox(hWnd, L"��������� �1", L"��������� ���������", MB_OK);
			break;
		case 1001:
			MessageBox(hWnd, L"��������� �2", L"��������� ���������", MB_OK);
			break;
		case 1002:
			MessageBox(hWnd, L"��������� �3", L"��������� ���������", MB_OK);
			break;
		default:
			break;
		}
		break;
	
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, lpCmdLine, wparam, lparam);
	}
}

LRESULT InitWindow(HINSTANCE hInstance, int nCmdShow)
{
	WNDCLASSEX wnd;
	wnd.cbSize = sizeof(WNDCLASSEX);
	wnd.cbClsExtra = 0;
	wnd.cbWndExtra = 0;
	wnd.hCursor = NULL;
	wnd.hIcon = NULL;
	wnd.hIconSm = NULL;
	wnd.hbrBackground = (HBRUSH)COLOR_WINDOW;
	wnd.hInstance = hInstance;
	wnd.lpfnWndProc = WndProc;
	wnd.lpszClassName = L"wnd";
	wnd.style = CS_HREDRAW | CS_VREDRAW;
	wnd.lpszMenuName = NULL;

	if (!RegisterClassEx(&wnd))
		return E_FAIL;

	hWnd = CreateWindow(L"wnd", L"Urok_1", WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, CW_USEDEFAULT, window_size_x, windows_size_y, hWnd, NULL, hInstance, NULL);
	
	CreateButton(hWnd, 10, 10,L"��������� �1",1000);
	CreateButton(hWnd, 10, 150, L"��������� �2",1001);
	CreateButton(hWnd, 10, 290, L"��������� �3",1002);

	if (!hWnd)
		return E_FAIL;

	UpdateWindow(hWnd);
	ShowWindow(hWnd, nCmdShow);
}

void CreateButton(HWND hWnd, int position_x, int position_y, LPCWSTR text,int idenitify)
{
	HWND hwndButton = CreateWindow(
		L"BUTTON",  // Predefined class; Unicode assumed 
		text,      // Button text 
		WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON,  // Styles 
		position_x,         // x position 
		position_y,         // y position 
		300,        // Button width
		100,        // Button height
		hWnd,     // Parent window
		(HMENU)idenitify,       // No menu.
		(HINSTANCE)GetWindowLong(hWnd, GWL_HINSTANCE),
		NULL);      // Pointer not needed.
}