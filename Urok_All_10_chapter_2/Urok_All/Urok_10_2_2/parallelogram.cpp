#include "parallelogram.h"

void paint_parallelogram(HWND hWnd, COLORREF color, POINT parallelogram[], int count_parallelogram, int iStyle, int window_x, int window_y, bool paint)
{
	HDC hDeviceContext;
	HPEN hPen;

	hDeviceContext = GetDC(hWnd);
	hPen = CreatePen(iStyle, 1, color);
	SelectObject(hDeviceContext, hPen);

	
	if (paint == true)
	{

		POINT brush_parallelogram = { 0,0 };
		MoveToEx(hDeviceContext, brush_parallelogram.x, brush_parallelogram.y, NULL);

		int schetchik = 0;      //������� � �������� �������������� �������

		while (brush_parallelogram.y <= window_y)
		{

			if ((((parallelogram[1].y - parallelogram[0].y)*(brush_parallelogram.x - parallelogram[0].x)) - ((parallelogram[1].x - parallelogram[0].x)*(brush_parallelogram.y - parallelogram[0].y)))<= 0 &&
				(((parallelogram[2].y - parallelogram[1].y)*(brush_parallelogram.x - parallelogram[1].x)) - ((parallelogram[2].x - parallelogram[1].x)*(brush_parallelogram.y - parallelogram[1].y)))<= 0 &&
				(((parallelogram[3].y - parallelogram[2].y)*(brush_parallelogram.x - parallelogram[2].x)) - ((parallelogram[3].x - parallelogram[2].x)*(brush_parallelogram.y - parallelogram[2].y)))<=0 &&
				(((parallelogram[0].y - parallelogram[3].y)*(brush_parallelogram.x - parallelogram[3].x)) - ((parallelogram[0].x - parallelogram[3].x)*(brush_parallelogram.y - parallelogram[3].y)))<=0)
			{
				//////////////////////////////////////////////////////////////////////////////////////////////////////
				//�������� ������� ������������ ������ ������, �������������� �������/////////////////////////////////
				//////////////////////////////////////////////////////////////////////////////////////////////////////
				if ((schetchik==0) && (((parallelogram[3].y - parallelogram[1].y)*(brush_parallelogram.x - parallelogram[1].x)) - ((parallelogram[3].x - parallelogram[1].x)*(brush_parallelogram.y - parallelogram[1].y))) >= 0)
				{
					hPen = CreatePen(iStyle, 1, RGB(255, 255, 0));
					SelectObject(hDeviceContext, hPen);
					schetchik++;
				}
				//////////////////////////////////////////////////////////////////////////////////////////////////////
				LineTo(hDeviceContext, brush_parallelogram.x+1, brush_parallelogram.y);
				brush_parallelogram.x++;
				MoveToEx(hDeviceContext, brush_parallelogram.x, brush_parallelogram.y, NULL);
			}
			else
			{
				if (brush_parallelogram.x >= window_x)
				{
					brush_parallelogram.x = 0;
					brush_parallelogram.y++;
					MoveToEx(hDeviceContext, brush_parallelogram.x, brush_parallelogram.y, NULL);
				}
				else
					brush_parallelogram.x++;
				MoveToEx(hDeviceContext, brush_parallelogram.x, brush_parallelogram.y, NULL);
			}
		}
	}
	else
	{

		MoveToEx(hDeviceContext, parallelogram[0].x, parallelogram[0].y, NULL);
		for (int i = 1; i < 4; i++)
			LineTo(hDeviceContext, parallelogram[i].x, parallelogram[i].y);
		LineTo(hDeviceContext, parallelogram[0].x, parallelogram[0].y);
	}

	ReleaseDC(hWnd, hDeviceContext);
	DeleteObject(hPen);
}

void paint_dotted_line(HWND hWnd, COLORREF color, POINT parallelogram[], int count_parallelogram,int vertex_one,int vertex_two, int iStyle)
{
	HDC hDeviceContext;
	HPEN hPen;

	hDeviceContext = GetDC(hWnd);
	hPen = CreatePen(iStyle, 0.9, color);
	SelectObject(hDeviceContext, hPen);

	MoveToEx(hDeviceContext, parallelogram[vertex_one].x, parallelogram[vertex_one].y, NULL);
    LineTo(hDeviceContext, parallelogram[vertex_two].x, parallelogram[vertex_two].y);

	ReleaseDC(hWnd, hDeviceContext);
	DeleteObject(hPen);
}