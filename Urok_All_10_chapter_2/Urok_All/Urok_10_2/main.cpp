#include <iostream>
#include <Windows.h>
#include <conio.h>
#include "parallelogram.h"

int main()
{
	HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
	CONSOLE_CURSOR_INFO curs;
	GetConsoleCursorInfo(hStdout, &curs);
	curs.bVisible = FALSE;
	SetConsoleCursorInfo(hStdout, &curs);

	HWND hWindow = NULL;
	COLORREF color[] = { RGB(255, 0, 0), RGB(255, 255, 0), RGB(0, 255, 0), RGB(155, 50, 0) };
	POINT parallelogram[] = {{ 120,150 }, { 450,150 }, { 450,300 }, { 120,300 }};
	POINT parallelogram_1[] = { { 450,150 },{ 550,50 },{ 550,200 },{ 450,300 } };
	POINT parallelogram_2[] = { { 120,150 },{ 220,50 },{ 220,200 },{ 120,300 } };
	POINT parallelogram_3[] = { { 220,50 },{ 550,50 },{ 550,200 },{ 220,200 } };

	int window_size_x = 700;
	int windows_size_y = 500;
	hWindow = GetConsoleWindow();
	SetWindowPos(hWindow, NULL, 1, 1, window_size_x, windows_size_y, NULL);
	

	paint_parallelogram(hWindow, color[0], parallelogram, 4, PS_SOLID, window_size_x, windows_size_y, true);
	paint_parallelogram(hWindow, color[0], parallelogram_1, 4, PS_SOLID, window_size_x, windows_size_y, true);
	paint_parallelogram(hWindow, color[0], parallelogram_2, 4, PS_SOLID, window_size_x, windows_size_y, true);
	paint_parallelogram(hWindow, color[0], parallelogram_3, 4, PS_DASH, window_size_x, windows_size_y, true);
	
	paint_parallelogram(hWindow, color[3], parallelogram, 4, PS_SOLID, window_size_x, windows_size_y, false);
	paint_parallelogram(hWindow, color[3], parallelogram_1, 4, PS_SOLID, window_size_x, windows_size_y, false);
	paint_parallelogram(hWindow, color[3], parallelogram_2, 4, PS_SOLID, window_size_x, windows_size_y, false);
	paint_parallelogram(hWindow, color[3], parallelogram_3, 4, PS_SOLID, window_size_x, windows_size_y, false);
	
	paint_dotted_line(hWindow, color[1], parallelogram_2, 4, 1, 2, PS_DASH);
	paint_dotted_line(hWindow, color[1], parallelogram_2, 4, 2, 3, PS_DASH);
	paint_dotted_line(hWindow, color[1], parallelogram_3, 4, 2, 3, PS_DASH);

	_getche();
	return 0;
}