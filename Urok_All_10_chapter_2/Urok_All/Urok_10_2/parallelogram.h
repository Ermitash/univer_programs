#pragma once
#include <iostream>
#include <Windows.h>
#include <conio.h>



void paint_parallelogram(HWND hWnd, COLORREF color,POINT parallelogram[],int count_parallelogram , int iStyle, int window_x,int window_y,bool paint);
void paint_dotted_line(HWND hWnd, COLORREF color, POINT parallelogram[], int count_parallelogram, int vertex_one, int vertex_two, int iStyle);