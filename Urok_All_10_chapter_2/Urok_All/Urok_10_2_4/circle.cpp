#include "circle.h"

void paint_circle(HWND hWnd, COLORREF color, POINT center, int radius, int iStyle,int window_x,int window_y, bool paint)
{
	HDC hDeviceContext;
	HPEN hPen;

	hDeviceContext = GetDC(hWnd);
	hPen = CreatePen(iStyle, 1, color);
	SelectObject(hDeviceContext, hPen);

	POINT coord = { 0,0 };
	MoveToEx(hDeviceContext, coord.x, coord.y, NULL);

	if (paint == true)
	{
		while (coord.y <= window_y)
		{
			if ((((coord.x - center.x)*(coord.x - center.x) + (coord.y - center.y)*(coord.y - center.y)) - radius*radius) < 0)
			{
				LineTo(hDeviceContext, coord.x + 1, coord.y);
				coord.x++;
				MoveToEx(hDeviceContext, coord.x, coord.y, NULL);
				
			}
			else
			{
				if (coord.x >= window_x)
				{
					coord.x = 0;
					coord.y++;
					MoveToEx(hDeviceContext, coord.x, coord.y, NULL);
				}
				else
					coord.x++;
				MoveToEx(hDeviceContext, coord.x, coord.y, NULL);
			}
		}
	}
	else
	{
		hPen = CreatePen(iStyle, 1, color);
		SelectObject(hDeviceContext, hPen);

		while (coord.y <= window_y)
		{
			if ((((coord.x - center.x)*(coord.x - center.x) + (coord.y - center.y)*(coord.y - center.y))- radius*radius)>=0 && (((coord.x - center.x)*(coord.x - center.x) + (coord.y - center.y)*(coord.y - center.y)) - radius*radius)<100)
			{
				LineTo(hDeviceContext, coord.x + 1, coord.y);
				coord.x++;
				MoveToEx(hDeviceContext, coord.x, coord.y, NULL);
			}
			else
			{
				if (coord.x >= window_x)
				{
					coord.x = 0;
					coord.y++;
					MoveToEx(hDeviceContext, coord.x, coord.y, NULL);
				}
				else
					coord.x++;
				MoveToEx(hDeviceContext, coord.x, coord.y, NULL);
			}
		}
	}

	ReleaseDC(hWnd, hDeviceContext);
	DeleteObject(hPen);
}