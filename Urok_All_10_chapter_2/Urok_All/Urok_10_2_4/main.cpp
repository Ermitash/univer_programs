#include <iostream>
#include <Windows.h>
#include <conio.h>
#include "circle.h"

int main()
{
	HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
	CONSOLE_CURSOR_INFO curs;
	GetConsoleCursorInfo(hStdout, &curs);
	curs.bVisible = FALSE;
	SetConsoleCursorInfo(hStdout, &curs);

	HWND hWindow = NULL;
	COLORREF color[] = { RGB(255, 255, 255), RGB(0, 0, 0) };

	int window_size_x = 700;
	int windows_size_y = 500;
	hWindow = GetConsoleWindow();
	SetWindowPos(hWindow, NULL, 1, 1, window_size_x, windows_size_y, NULL);

	POINT center = { 250,245 };
	paint_circle(hWindow, color[0], center, 200, PS_SOLID, window_size_x, windows_size_y, true);
	paint_circle(hWindow, color[1], center, 200-200*0.1, PS_SOLID, window_size_x, windows_size_y, true);
	paint_circle(hWindow, color[0], center, 200 - 200 * 0.2, PS_SOLID, window_size_x, windows_size_y, true);
	paint_circle(hWindow, color[1], center, 200 - 200 * 0.3, PS_SOLID, window_size_x, windows_size_y, true);
	paint_circle(hWindow, color[0], center, 200 - 200 * 0.4, PS_SOLID, window_size_x, windows_size_y, true);
	paint_circle(hWindow, color[1], center, 200 - 200 * 0.5, PS_SOLID, window_size_x, windows_size_y, true);
	paint_circle(hWindow, color[0], center, 200 - 200 * 0.6, PS_SOLID, window_size_x, windows_size_y, true);
	paint_circle(hWindow, color[1], center, 200 - 200 * 0.7, PS_SOLID, window_size_x, windows_size_y, true);
	paint_circle(hWindow, color[0], center, 200 - 200 * 0.8, PS_SOLID, window_size_x, windows_size_y, true);
	paint_circle(hWindow, color[1], center, 200 - 200 * 0.9, PS_SOLID, window_size_x, windows_size_y, true);

	COORD position = { 10,20 };

	SetConsoleTextAttribute(hStdout, 11);
	SetConsoleCursorPosition(hStdout, position);
	std::cout << "2";
	position = { 15,20 };
	SetConsoleCursorPosition(hStdout, position);
	std::cout << "4";
    position = { 20,20 };
	SetConsoleCursorPosition(hStdout, position);
	std::cout << "6";
	position = { 25,20 };
	SetConsoleCursorPosition(hStdout, position);
	std::cout << "8";
	position = { 30,20 };
	SetConsoleCursorPosition(hStdout, position);
	std::cout << "10";
	position = { 37,20 };
	SetConsoleCursorPosition(hStdout, position);
	std::cout << "8";
	position = { 42,20 };
	SetConsoleCursorPosition(hStdout, position);
	std::cout << "6";
	position = { 47,20 };
	SetConsoleCursorPosition(hStdout, position);
	std::cout << "4";
	position = { 52,20 };
	SetConsoleCursorPosition(hStdout, position);
	std::cout << "2";

	_getche();
	return 0;
}