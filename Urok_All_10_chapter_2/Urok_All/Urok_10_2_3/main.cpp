#include <iostream>
#include <Windows.h>
#include <conio.h>
#include "parallelogram.h"

int main()
{
	HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
	CONSOLE_CURSOR_INFO curs;
	GetConsoleCursorInfo(hStdout, &curs);
	curs.bVisible = FALSE;
	SetConsoleCursorInfo(hStdout, &curs);

	HWND hWindow = NULL;
	COLORREF color[] = { RGB(255, 0, 0), RGB(255, 255, 0), RGB(0, 255, 0), RGB(155, 50, 0) };
	POINT parallelogram[] = { { 100,150 },{ 250,50 },{ 400,150 },{ 250,250 } };
	POINT parallelogram_1[] = { { 250,250 },{ 400,150 },{ 400,300 },{ 250,400 } };
	POINT parallelogram_2[] = { { 100,150 },{ 250,250 },{ 250,400 },{ 100,300 } };

	int window_size_x = 700;
	int windows_size_y = 500;
	hWindow = GetConsoleWindow();
	SetWindowPos(hWindow, NULL, 1, 1, window_size_x, windows_size_y, NULL);


	paint_parallelogram(hWindow, color[0], parallelogram, 4, PS_SOLID, window_size_x, windows_size_y, true);
	paint_parallelogram(hWindow, color[1], parallelogram_1, 4, PS_SOLID, window_size_x, windows_size_y, true);
	paint_parallelogram(hWindow, color[2], parallelogram_2, 4, PS_SOLID, window_size_x, windows_size_y, true);


	_getche();
	return 0;
}