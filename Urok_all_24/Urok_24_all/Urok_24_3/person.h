#pragma once
#include <iostream>
#include <string>
using namespace std;

class person
{
public:
	person();
	person(string one, string two);
	friend bool operator < (const person &one, const person &two);
	friend bool operator == (const person &one, const person &two);
	void display(string tel) const;
private:
	string firstname;
	string secondname;
};

