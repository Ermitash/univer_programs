#include "person.h"
#include <iostream>
#include <Windows.h>
#include <functional>
#include <map>
#include <string>
using namespace std;

void show_baza(multimap<person, string, less<person> > &baza)
{
	multimap<person, string, less<person> >::iterator iter;
	iter = baza.begin();

	while (iter != baza.end())
	{
		iter->first.display(iter->second);
		iter++;
	}
}

void get_information_baze(multimap<person, string, less<person> > &baza_1)
{
	string last_name = " ";
	string first_name = " ";
	multimap<person, string, less<person> >::iterator iter;
	cout << "���������� ������� �������  � ���: ";
	cin >> last_name >> first_name;
	cout << endl << endl;
	person vremenno(last_name, first_name);

	iter = baza_1.lower_bound(vremenno);

	while (iter != baza_1.upper_bound(vremenno))
	{
		iter->first.display(iter->second);
		iter++;
	}
}

void delete_information(multimap<person, string, less<person> > &baza_1)
{
	string last_name = " ";
	string first_name = " ";
	multimap<person, string, less<person> >::iterator iter;
	cout << "��������:" << endl;
	cout << "���������� ������� �������  � ���: ";
	cin >> last_name>>first_name;
	cout << endl << endl;
	string numb;
	cout << "���������� ������� ����� ��������: ";
	cin >> numb;
	cout << endl << endl;
	person vremenno(last_name, first_name);

	iter = baza_1.lower_bound(vremenno);

	int kol = 0;
	while (iter != baza_1.upper_bound(vremenno) && iter->second==numb)
	{
		baza_1.erase(iter);
		iter = baza_1.lower_bound(vremenno);
		kol++;
	}
	
	cout << "������� " << kol << " �������" << endl;

	cout << "������� ����� ����� ��������: ";
	cin >> numb;

	for (int i = 0; i < kol; i++)
	{
		baza_1.insert(make_pair(vremenno, numb));
	}
	
}

multimap<person, string, less<person> > baza;

int main()
{
	SetConsoleOutputCP(1251);
	SetConsoleCP(1251);

	person *Ptr_Persons[6];
    Ptr_Persons[0]= new person("������", "�������");
	Ptr_Persons[1] = new person("�������", "�������");
	Ptr_Persons[2] = new person("������", "����");
	Ptr_Persons[3] = new person("������", "�������");
	Ptr_Persons[4] = new person("�������", "�������");
	Ptr_Persons[5] = new person("������", "���������");

	string nums[] = { "555-453454","456-456432", "123-890567", "345-876401", "381-563936", "529-199345" };
	
	for (int i = 0; i < 6; i++)
	{
		baza.insert(make_pair(*Ptr_Persons[i], nums[i]));
	}
	
	show_baza(baza);
	cout << endl;
	get_information_baze(baza);
	delete_information(baza);
	cout << endl<<endl;
	show_baza(baza);

	system("pause");
	return 0;
}