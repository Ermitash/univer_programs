#include "person.h"

bool operator<(const person & one, const person & two)
{
	if (one.secondname == two.secondname)
		return (one.firstname < two.firstname) ? true : false;
	return (one.secondname<two.secondname)?true: false;
}

bool operator==(const person & one, const person & two)
{
	return (one.secondname == two.secondname && one.firstname == two.firstname) ?  true : false;
}

person::person()
	: secondname(" "),firstname(" ")
{ }

person::person(string one, string two)
	: secondname(one), firstname(two)
{ }

void person::display(string tel) const
{
	cout << secondname << " " << firstname << " " << tel << endl;
}
