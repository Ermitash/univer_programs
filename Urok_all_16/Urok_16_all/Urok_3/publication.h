#pragma once
#include <iostream>
#include <string>
using namespace std;


class publication
{
public:
	publication();
	void setPrice(float Price);
	float getPrice();
	void setTitle(string Title);
	string getTitle();
private:
	string Title;
	float Price;

};