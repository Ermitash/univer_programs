#pragma once
#include <iostream>
#include <Windows.h>
#include <conio.h>

class circle
{
public:
	circle(COLORREF color, POINT center, double radius=100);
	void set_circle(double radius, POINT center, COLORREF color);
	void draw_circle(HWND hWnd,int iStyle, int window_x, int window_y, bool paint, HDC hDeviceContext, HPEN hPen);
	POINT get_center();
	double get_radius();
	COLORREF get_color();
private:
	POINT center;
	double radius;
	COLORREF color;

};