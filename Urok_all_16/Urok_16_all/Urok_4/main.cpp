#include <Windows.h>
#include <iostream>
#include "ellipse.h"

HWND hWnd=NULL;
int window_size_x = 1366;
int windows_size_y = 768;
LRESULT CALLBACK WndProc(HWND hWnd,UINT lpCmdLine,WPARAM wparam,LPARAM lparam);
LRESULT InitWindow(HINSTANCE hInstance, int nCmdShow);
HINSTANCE hInstance_2 = NULL;

int WinMain(HINSTANCE hInstance, HINSTANCE PrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	hInstance_2 = hInstance;
	InitWindow( hInstance,nCmdShow);

	MSG msg = { 0 };

	while (msg.message!=WM_QUIT)
	{
		if (GetMessage(&msg, NULL,0, 0) != 0)
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	

	return (int)msg.wParam;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT lpCmdLine, WPARAM wparam, LPARAM lparam)
{
	PAINTSTRUCT ps;
	HDC hDeviceContext=NULL;
	HPEN hPen=NULL;
	COLORREF color[] = { RGB(255, 0, 0), RGB(0, 255, 0),RGB(0,0,255) };
	POINT center = { 600.0,300.0 };
	double radius = 180;
	double radius2 = 100;
	ellipse *ellipse1 = new ellipse(color[0], center, radius, radius2);
	switch (lpCmdLine)
	{
	case WM_PAINT:
	    hDeviceContext = BeginPaint(hWnd, &ps);
		ellipse1->draw_ellipse(hWnd, PS_SOLID, window_size_x, windows_size_y, true, hDeviceContext, hPen);
		center = { 800,350 };
		ellipse1->set_ellipse(color[1], center, radius + 50, radius2 + 250);
		ellipse1->draw_ellipse(hWnd, PS_SOLID, window_size_x, windows_size_y, true, hDeviceContext, hPen);
		center = { 500,500 };
		ellipse1->set_ellipse(color[2], center, radius + 100, radius2 + 150);
		ellipse1->draw_ellipse(hWnd, PS_SOLID, window_size_x, windows_size_y, false, hDeviceContext, hPen);
		center = { 600,600 };
		ellipse1->set_ellipse(color[0], center, radius + 50, radius2 + 50);
		ellipse1->draw_ellipse(hWnd, PS_SOLID, window_size_x, windows_size_y, false, hDeviceContext, hPen);

		EndPaint(hWnd, &ps);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		ReleaseDC(hWnd, hDeviceContext);
		DeleteObject(hPen);
		break;
	default:
		return DefWindowProc(hWnd, lpCmdLine, wparam, lparam);
	}
}

LRESULT InitWindow(HINSTANCE hInstance, int nCmdShow)
{
	WNDCLASSEX wnd;
	wnd.cbSize = sizeof(WNDCLASSEX);
	wnd.cbClsExtra = 0;
	wnd.cbWndExtra = 0;
	wnd.hCursor = NULL;
	wnd.hIcon = NULL;
	wnd.hIconSm = NULL;
	wnd.hbrBackground = (HBRUSH)COLOR_WINDOW;
	wnd.hInstance = hInstance;
	wnd.lpfnWndProc = WndProc;
	wnd.lpszClassName = "wnd";
	wnd.style = CS_HREDRAW | CS_VREDRAW;
	wnd.lpszMenuName = NULL;

	if (!RegisterClassEx(&wnd))
		return E_FAIL;

	hWnd = CreateWindow("wnd", "Urok_1", WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, CW_USEDEFAULT, window_size_x, windows_size_y, hWnd, NULL, hInstance, NULL);

	if (!hWnd)
		return E_FAIL;

	UpdateWindow(hWnd);
	ShowWindow(hWnd, nCmdShow);
}