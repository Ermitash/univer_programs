#pragma once
#include "circle.h"

class ellipse :circle
{
public:
	ellipse(COLORREF color, POINT center, double radius1,double radius2);
	ellipse(COLORREF color, POINT center, double radius2);
	void set_ellipse(COLORREF color, POINT center, double radius1, double radius2);
	void draw_ellipse(HWND hWnd, int iStyle, int window_x, int window_y, bool paint, HDC hDeviceContext, HPEN hPen);
private:
	double radius2;
};
