#include "ellipse.h"

ellipse::ellipse(COLORREF color, POINT center, double radius1,double radius2)
	:circle(color,center,radius1)
{
	ellipse::radius2 = radius2;
}

ellipse::ellipse(COLORREF color, POINT center, double radius2)
	:circle(color, center)
{
	ellipse::radius2 = radius2;
}

void ellipse::set_ellipse(COLORREF color, POINT center, double radius1, double radius2)
{
	set_circle(radius1, center, color);
	ellipse::radius2 = radius2;
}

void ellipse::draw_ellipse(HWND hWnd, int iStyle, int window_x, int window_y, bool paint, HDC hDeviceContext, HPEN hPen)
{
	hDeviceContext = GetDC(hWnd);
	hPen = CreatePen(iStyle, 1, get_color());
	SelectObject(hDeviceContext, hPen);

	POINT coord = { 0.0,0.0 };
	MoveToEx(hDeviceContext, coord.x, coord.y, NULL);

	if (paint == true)
	{
		while (coord.y <= window_y)
		{
			
			if ((((coord.x - get_center().x)*(coord.x - get_center().x)*(radius2*radius2) +((coord.y - get_center().y)*(coord.y - get_center().y))*(get_radius()*get_radius())) - get_radius()*get_radius()*radius2*radius2) < 0)
			{
				LineTo(hDeviceContext, coord.x + 1, coord.y);
				coord.x++;
				MoveToEx(hDeviceContext, coord.x, coord.y, NULL);
			}
			else
			{
				if (coord.x >= window_x)
				{
					coord.x = 0;
					coord.y++;
					MoveToEx(hDeviceContext, coord.x, coord.y, NULL);
				}
				else
					coord.x++;
				MoveToEx(hDeviceContext, coord.x, coord.y, NULL);
			}
		}
	}
	else
	{
		hPen = CreatePen(iStyle, 1, get_color());
		SelectObject(hDeviceContext, hPen);

		while (coord.y <= window_y)
		{
			if ((((coord.x - get_center().x)*(coord.x - get_center().x)*(radius2*radius2) + ((coord.y - get_center().y)*(coord.y - get_center().y))*(get_radius()*get_radius())) - get_radius()*get_radius()*radius2*radius2) >= 0 && (((coord.x - get_center().x)*(coord.x - get_center().x)*(radius2*radius2) + ((coord.y - get_center().y)*(coord.y - get_center().y))*(get_radius()*get_radius())) - get_radius()*get_radius()*radius2*radius2) <100000000)
			{
				LineTo(hDeviceContext, coord.x + 1, coord.y);
				coord.x++;
				MoveToEx(hDeviceContext, coord.x, coord.y, NULL);
			}
			else
			{
				if (coord.x >= window_x)
				{
					coord.x = 0;
					coord.y++;
					MoveToEx(hDeviceContext, coord.x, coord.y, NULL);
				}
				else
					coord.x++;
				MoveToEx(hDeviceContext, coord.x, coord.y, NULL);
			}
		}
	}
}