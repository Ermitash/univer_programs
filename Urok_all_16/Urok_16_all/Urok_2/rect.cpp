#include "rect.h"

void rect::set_rect(COORD left_bottom, COORD right_top, color color_type)
{
	rect::left_bottom = left_bottom;
	rect::right_top = right_top;
	rect::color_type = color_type;
}

void rect::draw_rect()
{

	for (int i = right_top.Y; i <= left_bottom.Y; i++)
	{
		MoveToEx(hdc, left_bottom.X, i, NULL);
		LineTo(hdc, left_bottom.X + (right_top.X-left_bottom.X), i);

	}

}

rect::rect(COORD height, COORD width)
{

	hwnd = GetDesktopWindow();
	hdc = GetWindowDC(hwnd);
	
	size_screen.X = GetSystemMetrics(SM_CXSCREEN);
	size_screen.Y = GetSystemMetrics(SM_CYSCREEN);

	COORD left_b;
	COORD right_t;

	left_b = { 0 ,size_screen.Y };
	right_t = { size_screen.X, 0 };

	color fon = { 0,0,255 };
	set_rect(left_b, right_t, fon);
    hPen = CreatePen(PS_SOLID, 0.9, RGB(fon.R, fon.G, fon.B));
	SelectObject(hdc, hPen);
	draw_rect();

	left_b.X = size_screen.X / 2 - width.X / 2;
	left_b.Y = size_screen.Y / 2 + height.Y / 2;
	right_t.X= size_screen.X / 2 + width.X / 2;
	right_t.Y = size_screen.Y / 2 - height.Y / 2;

	set_rect(left_b, right_t, fon);
}

rect::~rect()
{
	ReleaseDC(hwnd, hdc);
	DeleteObject(hPen); 
}