#include "Move_rect.h"


move_rect::move_rect(COORD height, COORD width)
	:rect(height,width)
{

}

move_rect::move_rect()
	: rect()
{

}


void move_rect::increase(color* color_type)
{
	COORD left_b = left_bottom;
	COORD right_t = right_top;

	left_b.X -= 126;
	left_b.Y += 77;
	right_t.X += 126;
	right_t.Y -= 77;


	set_rect(left_b, right_t, *color_type);
	hPen = CreatePen(PS_SOLID, 0.9, RGB(color_type->R, color_type->G, color_type->B));
	SelectObject(hdc, hPen);
	draw_rect();
}

void move_rect::decrease(color* color_type)
{

	COORD left_b = left_bottom;
	COORD right_t = right_top;

	left_b.X += 126;
	left_b.Y -= 77;
	right_t.X -= 126;
	right_t.Y += 77;

	set_rect(left_b, right_t, *color_type);
	hPen = CreatePen(PS_SOLID, 0.9, RGB(color_type->R, color_type->G, color_type->B));
	SelectObject(hdc, hPen);
	draw_rect();
}
