#include <Windows.h>
#include <iostream>
#include "Move_rect.h"

HWND hWnd = NULL;
int window_size_x = 1366;
int windows_size_y = 768;
LRESULT CALLBACK WndProc(HWND hWnd, UINT lpCmdLine, WPARAM wparam, LPARAM lparam);
LRESULT InitWindow(HINSTANCE hInstance, int nCmdShow);

int WinMain(HINSTANCE hInstance, HINSTANCE PrevInstance, LPSTR lpCmdLine, int nCmdShow)
{

	InitWindow(hInstance, nCmdShow);

	MSG msg = { 0 };

	while (msg.message != WM_QUIT)
	{
		if (GetMessage(&msg, NULL, 0, 0) != 0)
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}



	return (int)msg.wParam;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT lpCmdLine, WPARAM wparam, LPARAM lparam)
{
	color color[5];
	int R = 100;
	int G = 100;
	int B = 100;

	for (int i = 0; i < 5; i++)
	{
		if(i==0)
		color[i].set_color(R, G, B);
		else
		{
			R += 100;
			G += 25;
			B += 25;
			color[i].set_color(R, G, B);
		}
	}
	move_rect* fd;
	

	switch (lpCmdLine)
	{
	case WM_KEYDOWN:
		DestroyWindow(hWnd);
		break;
	case WM_PAINT:
		 fd=new move_rect();
		 for (int i = 0; i < 5; i++)
		 {
			 Sleep(300);
			 fd->increase(&color[i]);
		 }
		 for (int i = 3; i >-1; i--)
		 {
			 Sleep(300);
			 fd->decrease(&color[i]);
		 }
		 Sleep(300);
		 delete fd;
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, lpCmdLine, wparam, lparam);
	}
}

LRESULT InitWindow(HINSTANCE hInstance, int nCmdShow)
{
	WNDCLASSEX wnd;
	wnd.cbSize = sizeof(WNDCLASSEX);
	wnd.cbClsExtra = 0;
	wnd.cbWndExtra = 0;
	wnd.hCursor = NULL;
	wnd.hIcon = NULL;
	wnd.hIconSm = NULL;
	wnd.hbrBackground = (HBRUSH)COLOR_WINDOW;
	wnd.hInstance = hInstance;
	wnd.lpfnWndProc = WndProc;
	wnd.lpszClassName = "wnd";
	wnd.style = CS_HREDRAW | CS_VREDRAW;
	wnd.lpszMenuName = NULL;

	if (!RegisterClassEx(&wnd))
		return E_FAIL;

	hWnd = CreateWindow("wnd", "Urok_1", WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, CW_USEDEFAULT, window_size_x, windows_size_y, hWnd, NULL, hInstance, NULL);

	if (!hWnd)
		return E_FAIL;

	ShowWindow(hWnd, nCmdShow);
}