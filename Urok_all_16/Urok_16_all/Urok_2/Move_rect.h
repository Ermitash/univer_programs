#pragma once
#include "rect.h"
#include <iostream>
#include <Windows.h>
using namespace std;

class move_rect :rect
{
public:
	move_rect(COORD height, COORD width);
	move_rect();
	void increase(color* color_type);
	void decrease(color* color_type);
};