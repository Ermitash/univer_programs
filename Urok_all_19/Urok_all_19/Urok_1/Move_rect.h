#pragma once
#include "rect.h"
#include <iostream>
#include <Windows.h>
using namespace std;

class move_rect : public rect
{
public:
	move_rect(HWND hwnd, HDC hDeviceContext, HPEN hPen, COORD center, color color, COORD height, COORD width);
	void increase(color* color_type);
	void decrease(color* color_type);
};