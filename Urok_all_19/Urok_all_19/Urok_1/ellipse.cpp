#include "ellipse.h"

ellipse::ellipse(HWND hwnd, HDC hDeviceContext, HPEN hPen, int window_x, int window_y, color color, COORD center, double radius, double radius2)
	:circle(hwnd,hDeviceContext,hPen,window_x,window_y,color,center,radius)
{
	set_radius2(radius2);
}

ellipse::ellipse(HWND hwnd, HDC hDeviceContext, HPEN hPen, int window_x, int window_y, color color, COORD center, double radius2)
	:circle(hwnd, hDeviceContext, hPen, window_x, window_y, color, center)
{
	set_radius2(radius2);
}

void ellipse::set_ellipse(color color, COORD center, double radius1, double radius2)
{
	set_circle(radius1, center, color);
	set_radius2(radius2);
}

void ellipse::set_radius2(double radius2)
{
	ellipse::radius2 = radius2;
}

double ellipse::get_radius2()
{
	return radius2;
}

void ellipse::draw()
{
	setHDC(GetDC(hwnd));
	sethPen(CreatePen(getiStyle(), 1, RGB(getColor().R, getColor().G, getColor().B)));
	SelectObject(getHDC(), gethPen());


	COORD coord = { 0.0,0.0 };
	MoveToEx(getHDC(), coord.X, coord.Y, NULL);

	if (getPaint() == true)
	{
		while (coord.Y <= getWindowY())
		{
			
			if ((((coord.X - getCenter().X)*(coord.X - getCenter().X)*(radius2*radius2) +((coord.Y - getCenter().Y)*(coord.Y - getCenter().Y))*(get_radius()*get_radius())) - get_radius()*get_radius()*radius2*radius2) < 0)
			{
				LineTo(getHDC(), coord.X + 1, coord.Y);
				coord.X++;
				MoveToEx(getHDC(), coord.X, coord.Y, NULL);
			}
			else
			{
				if (coord.X >= getWindowX())
				{
					coord.X = 0;
					coord.Y++;
					MoveToEx(getHDC(), coord.X, coord.Y, NULL);
				}
				else
					coord.X++;
				MoveToEx(getHDC(), coord.X, coord.Y, NULL);
			}
		}
	}
	else
	{
		sethPen(CreatePen(getiStyle(), 1, RGB(getColor().R, getColor().G, getColor().B)));
		SelectObject(getHDC(), gethPen());

		while (coord.Y <= getWindowY())
		{
			if ((((coord.X - getCenter().X)*(coord.X - getCenter().X)*(radius2*radius2) + ((coord.Y - getCenter().Y)*(coord.Y - getCenter().Y))*(get_radius()*get_radius())) - get_radius()*get_radius()*radius2*radius2) >= 0 && (((coord.X - getCenter().X)*(coord.X - getCenter().X)*(radius2*radius2) + ((coord.Y - getCenter().Y)*(coord.Y - getCenter().Y))*(get_radius()*get_radius())) - get_radius()*get_radius()*radius2*radius2) <100000000)
			{
				LineTo(getHDC(), coord.X + 1, coord.Y);
				coord.X++;
				MoveToEx(getHDC(), coord.X, coord.Y, NULL);
			}
			else
			{
				if (coord.X >= getWindowX())
				{
					coord.X = 0;
					coord.Y++;
					MoveToEx(getHDC(), coord.X, coord.Y, NULL);
				}
				else
					coord.X++;
				MoveToEx(getHDC(), coord.X, coord.Y, NULL);
			}
		}
	}
}