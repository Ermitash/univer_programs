#pragma once
#include "circle.h"

class ellipse :public circle
{
public:
	ellipse(HWND hwnd, HDC hDeviceContext, HPEN hPen, int window_x, int window_y, color color, COORD center, double radius, double radius2);
	ellipse(HWND hwnd, HDC hDeviceContext, HPEN hPen, int window_x, int window_y, color color, COORD center, double radius2);
	void set_ellipse(color color, COORD center, double radius1, double radius2);
	virtual void draw();
	void set_radius2(double radius2);
	double get_radius2();
private:
	double radius2;
};
