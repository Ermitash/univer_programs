#pragma once
#include <iostream>
using namespace std;

class time
{
	friend ostream &operator<<(ostream &s, const time &one);
public:
	time();
	time(unsigned int hrs, unsigned int minuts, unsigned int seconds);
	time operator+(const time & one);
	time operator-(const time & one);
private:
	unsigned int hrs;
	unsigned int minuts;
	unsigned int seconds;
};