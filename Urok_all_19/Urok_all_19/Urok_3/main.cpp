#include <iostream>
#include "Time.h"
using namespace std;

int main()
{
	setlocale(LC_ALL, "Russian");
	time time1(3,12,45);
	time time2(6, 45, 32);
	time time3;
	time time4;
	time3 = time1 + time2;
	time4 = time2 - time1;

	cout << time1 << endl;
	cout << time2 << endl;
	cout << time3 << endl;
	cout << time4 << endl;
	system("pause");
	return 0;
}