#include "Time.h"

time::time()
{
	hrs = 0;
	minuts = 0;
	seconds = 0;
}

time::time(unsigned int hrs, unsigned int minuts, unsigned int seconds)
{
	if (hrs < 0 || minuts < 0 || seconds < 0)
	{
		cout << "����� �� ����� ���� ������������� (�����������)!" << endl;
		system("pause");
		exit(1);
	}
	time::hrs = hrs;
	time::minuts = minuts;
	time::seconds = seconds;
}

time time::operator+(const time & one)
{
	time two(this->hrs, this->minuts, this->seconds);
	two.hrs += one.hrs;
	two.minuts += one.minuts;
	two.seconds += one.seconds;

	while (true)
	{
		if (two.seconds >= 60)
		{
			two.seconds -= 60;
			two.minuts++;
		}
		else if (two.minuts >= 60)
		{
			two.minuts -= 60;
			two.hrs++;
		}
		else if (two.hrs >= 24)
			two.hrs = 0;
		else
			break;
	}
	return two;
}

time time::operator-(const time & one)
{
	if ((this->hrs*60*60+this->minuts*60+this->seconds) < (one.hrs*60*60+one.minuts*60+one.seconds))
	{
		cout << "����� �� ����� ���� �������������!" << endl;
		system("pause");
		exit(1);
	}
	time two(this->hrs, this->minuts , this->seconds);

	int i = (two.hrs * 60 * 60 + two.minuts * 60 + two.seconds) - (one.hrs * 60 * 60 + one.minuts * 60 + one.seconds);
	
	two.hrs = 0; two.minuts = 0; two.seconds = 0;

	while (true)
	{
		if (i - 60 >= 0)
		{
			i -= 60;
			two.minuts++;
		}
		else
		{
			two.seconds = i;

			if (two.minuts >= 60)
			{
				two.minuts -= 60;
				two.hrs++;
			}
			else
			{
				if (two.hrs >= 24)
					two.hrs = 0;
				else
				break;
			}
		}
	}

	return two;
}

ostream & operator<<(ostream & s, const time & one)
{
	s << one.hrs << ":" << one.minuts << ":" << one.seconds;
	return s;
}
