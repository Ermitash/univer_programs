#pragma once
#include <iostream>
#include <Windows.h>
using namespace std;

struct color
{
	void set_color(int R,int G,int B)
	{
	   this->R = R;
	   this->G = G;
	   this->B = B;
	}
	int R;
	int G;
	int B;
};

class rect
{
	friend void increase(rect *a, color* color_type);
	friend void decrease(rect *a, color* color_type);
protected:
	HWND hwnd;
	HDC hdc;
	HPEN hPen;

	COORD size_screen;
	COORD left_bottom;
	COORD right_top;
	color color_type;
	
public:
	rect(COORD height = { 0,153 }, COORD width = { 273,0 });
	void set_rect(COORD left_bottom, COORD right_top, color color_type);
	void draw_rect();
	~rect();
};